<?php if ((!function_exists("check_theme_footer") || !function_exists("check_theme_header"))) { ?><?php { /* nothing */ } ?><?php } else { ?>
<?php get_header(); ?>
<?php/* get_sidebar('left'); */?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<div id="content">
<div id="contentinner">
	<div class="single">
		<?php $post = get_post(get_the_ID()); ?>
		<div class="title">
			<h1><?= $post->post_title; ?></h1>
		</div>
		<?php homa_print_post($post->ID,true);?>
		
		<div id="single_paginate">
			<?php homa_paginate_single($post->ID); ?>
		</div>
	</div>
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?><?php } ?>