<?php
/*
Template Name: Archives
*/
?>

<?php get_header(); ?>

<?php get_sidebar('left'); ?>

<div id="content">
<div id="contentinner">
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>

<ul id="archives">
<li>
<h6>Архивы по месяцам:</h6>
<ul><?php wp_get_archives('type=monthly'); ?></ul>
<h6>Архивы по рубрикам:</h6>
<ul><?php wp_list_categories(); ?></ul>	
<h6>Просмотреть последние 50 записей:</h6>
<ul><?php query_posts('showposts=50'); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Постоянная ссылка на: <?php the_title(); ?>"><?php the_title(); ?></a></li>
<?php endwhile; else: endif; ?>
</ul>				
</li>
</ul><!-- ARCHIVES END -->

</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>