<?php get_header(); ?> 
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>

<?php
$category = get_the_category();
$current_cat = $category[0]->cat_ID;
?>

<?php wp_reset_query(); ?>

<?php
$term = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$myposts = get_posts($args = array(
	'posts_per_page' => 12,
	'paged' => $paged,
    'post_type' => 'foto',
	'order'=>'ASC',
    'tax_query' => array(
        array(
        'taxonomy' => 'albums',
        'field' => 'slug',
        'terms' => $term->slug)
    ))
);
query_posts($args);
//$query = new WP_Query( $args );

echo '<ul class="galery_pic"><div id="pic_wrapper">';

foreach ($myposts as $mypost) {
	echo '<li>';
    echo $mypost->post_content;
	echo '</li>';
}
echo '</div></ul>';
?>

<div class="clearfix"></div>

<?php get_footer(); ?>