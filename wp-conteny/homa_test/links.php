<?php
/*
Template Name: Links
*/
?>

<?php get_header(); ?>

<?php get_sidebar('left'); ?>

<div id="content">
<div id="contentinner">
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>

<ul id="links">
<li>
<ul>
<?php wp_list_bookmarks('title_before=<h6>&title_after=</h6>'); ?>
</ul>		
</li>
</ul><!-- LINKS END -->

</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>