<div id="sidebar">
<div id="sidebarinner">

<ul class="sidebar_list">
<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(1)) : ?>

<?php include (TEMPLATEPATH . '/includes/searchform.php'); ?>

<?php if(is_single()) { ?>
<?php include (TEMPLATEPATH . '/includes/related.php'); ?> 
<?php } ?>

<?php $featured_category_active = get_theme_option('featured_category_activate'); if(($featured_category_active == '') || ($featured_category_active == 'No')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<?php if((is_home()) && (is_front_page())) { ?>
<?php include (TEMPLATEPATH . '/includes/featured-category.php'); ?> 
<?php } ?> 
<?php } ?>

<?php $sponsor_activate = get_theme_option('sponsor_activate'); if(($sponsor_activate == '') || ($sponsor_activate == 'No')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<?php include (TEMPLATEPATH . '/includes/sponsor.php'); ?>
<?php } ?>

<?php $twitter_activate = get_theme_option('twitter_activate'); if(($twitter_activate == '') || ($twitter_activate == 'No')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<?php include (TEMPLATEPATH . '/includes/twitter.php'); ?> 
<?php } ?>

<?php $emvideo_activate = get_theme_option('emvideo_activate'); if(($emvideo_activate == '') || ($emvideo_activate == 'No')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<?php include (TEMPLATEPATH . '/includes/video.php'); ?>
<?php } ?>

<?php endif; ?>
</ul><!-- SIDEBARLIST END -->

<div id="sidebar-left">
<ul class="sidebar_list">
<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(2)) : ?>

<li class="widget_categories">
<h6><?php _e('Рубрики'); ?></h6>
<form action="<?php bloginfo('url'); ?>" method="get">
<?php
$select = wp_dropdown_categories('show_option_none=Выберите рубрику&show_count=1&orderby=name&echo=0');
$select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
echo $select;
?>
<noscript><input type="submit" value="View" /></noscript>
</form>
</li>


<li class="widget_pages">
<h6><?php _e('Страницы'); ?></h6>
<form action="<?php bloginfo('url'); ?>" method="get">
<?php
$select = wp_dropdown_pages('show_option_none=Выберите страницу&orderby=name&echo=0');
$select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
echo $select;
?>
<noscript><input type="submit" value="View" /></noscript>
</form>
</li>


<li class="widget_archive">
<h6><?php _e('Архивы'); ?></h6>
	<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
	<option value=""><?php echo attribute_escape(__('Выберите месяц')); ?></option> 
  	<?php wp_get_archives('type=monthly&format=option&show_post_count=1'); ?> </select>
</li>

<?php endif; ?>
</ul><!-- SIDEBARLIST END -->
</div><!-- SIDEBAR LEFT END -->

<div id="sidebar-right">
<ul class="sidebar_list">
<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(3)) : ?>



<li class="widget_recentcomments_gravatar">
<h6><?php _e('Недавние комментарии'); ?></h6>
<?php get_avatar_recent_comment(); ?>
</li>

<li class="widget_hottopics">
<h6><?php _e('Свежие новости'); ?></h6>
<?php get_hottopics(); ?>
</li>

<li class="widget_tag_cloud">
<h6><?php _e('Популярные метки'); ?></h6>
<div>
<?php if(function_exists("wp_tag_cloud")) { ?>
<?php wp_tag_cloud('smallest=8&largest=20&'); ?>
<?php } ?>
</div>
</li>


<?php endif; ?>
</ul><!-- SIDEBARLIST END -->
</div><!-- SIDEBAR RIGHT END -->

<div class="clearfix"></div>
</div><!-- SIDEBARINNER END -->
</div><!-- SIDEBAR END -->
