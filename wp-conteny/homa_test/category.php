<?php
	/*id головной сторінки*/
	$home_id = 2;
?>
<?php get_header(); ?>
<?php/* get_sidebar('left'); */?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<style>
.dddddsssss{
	border-bottom: 1px dashed #ccc;
}
.dddddsssss:last-child{
	border-bottom: none;
}
</style>
<div id="content">
<div id="contentinner">
	<?php
		/*$str1 = 'novini';
		$str2 = 'slidcho-operativna-diyalnist';*/

		$cat = get_the_category();
		$cat = get_category(get_query_var('cat'),false);
		//var_dump($cat);
	?>
	
	<div class="title">
		<h1><?= $cat->name; ?></h1>
	</div>
	
	<?php
		/* Запрос на количиство постов в категории */
		$count_all_post = $wpdb->get_results("SELECT $wpdb->term_taxonomy.count FROM $wpdb->term_taxonomy INNER JOIN $wpdb->terms ON $wpdb->term_taxonomy.term_id = $wpdb->terms.term_id WHERE ($wpdb->terms.slug = '$cat->category_nicename')");
		//$count_all_post = (int)$count_all_post[0]->count;
		
		
		/* Достаем все занписи с категории */
		$arr_query = $wpdb->get_results("
				SELECT $wpdb->posts.id, $wpdb->posts.post_title
				FROM $wpdb->term_relationships
					INNER JOIN $wpdb->posts ON $wpdb->term_relationships.object_id = $wpdb->posts.id 
					INNER JOIN $wpdb->term_taxonomy ON $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_id
					INNER JOIN $wpdb->terms ON $wpdb->term_taxonomy.term_id = $wpdb->terms.term_id
				WHERE ($wpdb->terms.slug = '$cat->category_nicename') ORDER BY $wpdb->posts.post_date DESC");
		
		$art_count = (int)$count_all_post[0]->count;	//для номера и счета статей(счет начинается с 1, а не с 0)
		$kil = get_field("kil_news", $home_id);			//количество выводимых статей(задайотся количиством колонок в админке)
		$kil_p = 1;										//количество видимих страниц возле активной с лева и с права, при пагинации
		$_page = 1;										//страница пагинации(пагинация начинается с 1, а не с 0)
		
		//$adres = get_bloginfo('home').'/category/novini/';
		$adres = get_bloginfo('home')."/category/$cat->slug/";
		
		/* В одной из категории находится лишняя запись */
		$pp = 0;
		if('1' === $arr_query[0]->id) $pp = 1;
		
		if($_GET['page']) $_page = $_GET['page'];
		
		/* Виводим неохадимые записи */
		for($item = $kil*$_page-$kil+$pp; $arr_query[$item] && $item < ($kil*$_page)+$pp;$item++){
			echo '<div class="post-meta dddddsssss" id="post-'.$arr_query[$item]->id.'">';
				homa_print_post($arr_query[$item]->id,false);
			echo '</div><!-- POST META '.$arr_query[$item]->id.' END -->';
		}
		
		/* Пагинация */
		/* В if исключаем ту сетуацию когда одна страница. */
		/* Пагинация не виводим когда одна страница. */
		if($art_count > $kil){
			$art_count /= $kil;
			$art_count = (int)ceil($art_count); /*Округляет дробь в большую сторону*/
		?>
			<div class="nomer_page">
			<?php
				/*В if проверяим нужны ли моментальные переходы на первою и последнею страницу*/
				if($art_count <= $kil_p + 1)
					for($i = 1; $i < $art_count+1; $i++) {
						echo "<a href='$adres?page=$i'";
						if($i==$_page) echo'class="select"';
						echo ">$i</a>";
					}
				/*В else если переходы всеже нужны*/
				else{
					/*Добавляем переход на первою страницу*/
					if(1 < $_page - $kil_p){
						echo "<a href='$adres'>1</a>";
						if(1 != $_page - $kil_p - 1){ echo "<span>...</span>"; }
					}
					for($i = 1; $i < $art_count+1; $i++) {
						if($i <= $_page + $kil_p && $i >= $_page - $kil_p){
							echo "<a href='$adres?page=$i'";
							if($i==$_page)echo 'class="select"';
							echo ">$i</a>";
						}
					}
					/*Добавляем переход на последнею страницу*/
					if($art_count > $_page + $kil_p) {
						if($art_count != $_page + $kil_p + 1)
							echo "<span>...</span>";
						echo "<a href='$adres?page=$art_count'>$art_count</a>";
					}
				}
			?>
			</div>
		<?php } ?>
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
