<div id="sidebar_right">
<div id="sidebarinner_right">

	<div id="sidebar_foto">
		<?php
		
			$args = array(
				'type'						=> 'post'
				,'child_of'					=> 0
				,'parent'					=> ''
				,'orderby'					=> 'date'
				,'order'					=> 'DESC'
				,'hide_empty'				=> 1
				,'hierarchical'				=> 1
				,'exclude'					=> ''
				,'include'					=> ''
				,'number'					=> 2
				,'taxonomy'					=> 'albums'
				,'pad_counts'				=> false
				,'posts_per_page'			=> -1
			);
			$categories = get_categories( $args );
			
			if( $categories ){
				$cnt = 0;
				echo '<div class="sidebar_right_post">';
				foreach( $categories as $cat ){
					echo '<div class="post-meta" id="post-'.$cnt.'">';
						if(!$cnt){
							echo '<div class="title_last_post">';
									echo '<a href="'.get_bloginfo('home').'/galereya/fotogalereya/">Фото</a>';
							echo '</div>';
						}
						echo '<div class="entry">';
							echo '<div class="post-info">';
								$taxonomy = $cat->taxonomy;
								$term_id = $cat->term_id;

								$thumbnail = get_field('album_pic', $taxonomy . '_' . $term_id);
								$url_a = home_url( '/' ).'?'.$taxonomy.'='.$cat->slug;
								echo '<a href="'.$url_a.'">';
										echo '<img src="'.$thumbnail['url'].'" alt="'.$cat->name.'"/>';
										//echo '<img src="'.get_bloginfo('template_directory').'/images/if_not_news_image.jpg" height="'.$height_img.'" width="'.$width_img.'"/>';
									echo '<p class="text">'.$cat->name.'</p>';
								echo '</a>';
							echo '</div><!-- POST INFO END -->';
							echo '<div class="clearleft"></div>';
						echo '</div><!-- ENTRY END -->';
					echo '</div><!-- POST META '.$post->ID.' END -->';
					$cnt++;
					if(2 == $cnt){
						echo '<a href="'.get_bloginfo('home').'/galereya/fotogalereya/" class="show_all">Всі альбоми >></a>';
						break;
					}
				}
				echo "</div>";
			}
		?>
	</div>
	<?php homa_print_last_post_type('video','Відео',2,'Всі відео >>');?>
	
<?/*<ul class="sidebar_list">
	<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(2)) : ?>
		<?php $sponsor_activate = get_theme_option('sponsor_activate'); if(($sponsor_activate == '') || ($sponsor_activate == 'No')) { ?>
			<?php { /* nothing *//* } ?>
		<?php } else { ?>
			<?php include (TEMPLATEPATH . '/includes/sponsor.php'); ?>
		<?php } ?>
		<?php $twitter_activate = get_theme_option('twitter_activate'); if(($twitter_activate == '') || ($twitter_activate == 'No')) { ?>
			<?php { /* nothing *//* } ?>
		<?php } else { ?>
			<?php include (TEMPLATEPATH . '/includes/twitter.php'); ?> 
		<?php } ?>
		<li class="widget_recentcomments_gravatar">
			<h6><?php _e('Недавние комментарии'); ?></h6>
			<?php get_avatar_recent_comment(); ?>
		</li>
		<li class="widget_hottopics">
			<h6><?php _e('Свежие новости'); ?></h6>
			<?php get_hottopics(); ?>
		</li>
		<li class="widget_tag_cloud">
			<h6><?php _e('Популярные метки'); ?></h6>
			<div>
				<?php if(function_exists("wp_tag_cloud")) { ?>
				<?php wp_tag_cloud('smallest=8&largest=20&'); ?>
				<?php } ?>
			</div>
		</li>
	<?php endif; ?>
	<div id="breadcrumbs"></div>
</ul><!-- SIDEBARLIST END -->*/?>
	
</div><!-- SIDEBARINNER END -->
</div><!-- SIDEBAR END -->
