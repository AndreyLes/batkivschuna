$(document).ready(function(){
	var w = $(".carousel-text").width();
	var wl = $(".carousel-button-left span").width();
	var wr = $(".carousel-button-right span").width();
	var wb = 1200 - w - wr - wl - 20;
	var wbp = wb+"px";
	wr = 1200 - wb;
	wl = wr + "px";
	w = w + "px";
	$(".carousel-block").css({'width': wbp});
	$(".carousel-wrapper").css({'margin-left': wl});
	$(".act").css({'left': w});
	wb = wb+20;
	wb = "-"+wb+"px";

	setTimeout(function fun() {
		$("#trans").css({'left': w});
		$(".carousel-items .carousel-block").eq(0).clone().appendTo(".carousel-items");
		$(".carousel-items").animate({'left': wb}, 1000);
		setTimeout(function () {
			$(".carousel-items .carousel-block").eq(0).remove();
			$(".carousel-items").css({"left":"0px"});
			$("#trans").css({'left': "-9999px"});
		}, 1100);
		setTimeout(fun,5000);
	}, 50000);

	$(".carousel-button-right").click(function(){
		$("#trans").css({'left': w});
		$(".carousel-items .carousel-block").eq(0).clone().appendTo(".carousel-items");
		$(".carousel-items").animate({'left': wb}, 1000);
		setTimeout(function () {
			$(".carousel-items .carousel-block").eq(0).remove();
			$(".carousel-items").css({"left":"0px"});
			$("#trans").css({'left': "-9999px"});
		}, 1100);
	});
	
	$(".carousel-button-left").click(function(){
		$("#trans").css({'left': w});
		$(".carousel-items .carousel-block:last").clone().prependTo(".carousel-items");
		$(".carousel-items").css({"left":wb});	
		$(".carousel-items").animate({'left': "0px"}, 1000);
		setTimeout(function () {
			$(".carousel-items .carousel-block:last").remove();
			$("#trans").css({'left': "-999px"});
		}, 1100);
	});
});