<ul class="sidebar_list">
<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(1)) : ?>
	<?php include (TEMPLATEPATH . '/includes/searchform.php'); ?>

	<?php if(is_single()) { ?>
		<?php include (TEMPLATEPATH . '/includes/related.php'); ?> 
	<?php } ?>

	<?php $featured_category_active = get_theme_option('featured_category_activate'); if(($featured_category_active == '') || ($featured_category_active == 'No')) { ?>
	<?php { /* nothing */ } ?>
	<?php } else { ?>
		<?php if((is_home()) && (is_front_page())) { ?>
		<?php include (TEMPLATEPATH . '/includes/featured-category.php'); ?> 
		<?php } ?> 
	<?php } ?>

	<li class="widget_categories">
	<h6>Рубрики</h6>
		<ul>
		<?php wp_list_categories('orderby=id&show_count=1&use_desc_for_title=0&title_li='); ?>
		</ul>
	</li>

	<li class="widget_pages">
	<h6>Страницы</h6>
		<ul>
		<?php wp_list_pages('title_li=&depth=0'); ?>
		</ul>
	</li>

	<li class="widget_archive">
	<h6>Архивы</h6>
		<ul>
		<?php wp_get_archives('type=monthly&limit=&show_post_count=1'); ?>
		</ul>
	</li>
<?php endif; ?>
</ul><!-- SIDEBARLIST END -->