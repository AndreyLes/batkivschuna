<?php/* Template Name: Всі новини */?>
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<div id="content">
<div id="contentinner">
	<div id="post-entry">
		<?php if (have_posts()):?>
			<? $post = get_post(get_the_ID());?>
			<div class="title">
				<h1><?= $post->post_title; ?></h1>
			</div><!-- POST INFO END -->
			<div class="page-content">
				<?php
				$arr_query = get_posts(array('numberposts' => -1));
				//var_dump($arr_query[10]->ID);
				$art_count = count($arr_query);	//для номера и счета статей(счет начинается с 1, а не с 0)
				//echo $art_count."<br>";
				$kil = 5;			//количество выводимых статей(задайотся количиством колонок в админке)
				$kil_p = 1;										//количество видимих страниц возле активной с лева и с права, при пагинации
				$_page = 1;										//страница пагинации(пагинация начинается с 1, а не с 0)
				$adres = get_bloginfo('home')."/vsi_novini/";
				
				$pp = 0;
				if('1' === $arr_query[0]->ID) $pp = 1;
				
				if($_GET['page']) $_page = $_GET['page'];
				
				/* Виводим неохадимые записи */
				for($item = $kil*$_page-$kil+$pp; $arr_query[$item] && $item < ($kil*$_page)+$pp;$item++){
					echo '<div class="post-meta" id="post-'.$arr_query[$item]->ID.'">';
						homa_print_post($arr_query[$item]->ID,false);
					echo '</div><!-- POST META '.$arr_query[$item]->ID.' END -->';
				}
		
					
				?>
			</div><!-- POST CONTENT END -->
			<div class="clearfix"></div>
		<?php endif; ?>
	</div><!-- POST ENTRY END -->
	<?
		
		
	/* Пагинация */
		/* В if исключаем ту сетуацию когда одна страница. */
		/* Пагинация не виводим когда одна страница. */
		if($art_count > $kil){
			$art_count /= $kil;
			$art_count = (int)ceil($art_count); /*Округляет дробь в большую сторону*/
		?>
			<div class="nomer_page">
			<?php
				/*В if проверяим нужны ли моментальные переходы на первою и последнею страницу*/
				if($art_count <= $kil_p + 1)
					for($i = 1; $i < $art_count+1; $i++) {
						echo "<a href='$adres?page=$i'";
						if($i==$_page) echo'class="select"';
						echo ">$i</a>";
					}
				/*В else если переходы всеже нужны*/
				else{
					/*Добавляем переход на первою страницу*/
					if(1 < $_page - $kil_p){
						echo "<a href='$adres'>1</a>";
						if(1 != $_page - $kil_p - 1){ echo "<span>...</span>"; }
					}
					for($i = 1; $i < $art_count+1; $i++) {
						if($i <= $_page + $kil_p && $i >= $_page - $kil_p){
							echo "<a href='$adres?page=$i'";
							if($i==$_page)echo 'class="select"';
							echo ">$i</a>";
						}
					}
					/*Добавляем переход на последнею страницу*/
					if($art_count > $_page + $kil_p) {
						if($art_count != $_page + $kil_p + 1)
							echo "<span>...</span>";
						echo "<a href='$adres?page=$art_count'>$art_count</a>";
					}
				}
			?>
			</div>
		<?php } ?>
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
