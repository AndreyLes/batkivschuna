<?php/* Template Name: Служба фінансових розслідувань */?>
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<div id="content">
<div id="contentinner">
	<div id="post-entry">
		<?php if (have_posts()):?>
			<? $post = get_post(get_the_ID());?>
			<div class="title">
				<h1><?= $post->post_title; ?></h1>
			</div><!-- POST INFO END -->
			<div class="page-content">
				<?php
					echo $post->post_content;
					$childrens = get_children( "numberposts=-1&post_parent=$post->ID" );
					foreach($childrens as $children):
						echo '<div class="post-meta" id="post-'.$_id.'">';
							homa_print_post($children->ID);
						echo '</div><!-- POST META '.$_id.' END -->';
					endforeach;
				?>
				
			</div><!-- POST CONTENT END -->
			<div class="clearfix"></div>
		<?php endif; ?>
	</div><!-- POST ENTRY END -->
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
