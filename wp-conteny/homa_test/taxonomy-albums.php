<?php get_header(); ?> 

<?php wp_reset_query(); ?>

<?php
	$term = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$myposts = get_posts($args = array(
		'posts_per_page' => 12,
		'paged' => $paged,
		'post_type' => 'foto',
		'order'=>'ASC',
		'tax_query' => array(
			array(
			'taxonomy' => 'albums',
			'field' => 'slug',
			'terms' => $term->slug)
		))
	);
?>

<div id="breadcrumbs">
	<?php $p = get_post(37);?>
	<?php $p2 = get_post(61);?>
	<a href="<?php bloginfo('home'); ?>" title="Головна">Головна</a>
	&raquo;
	<a href="<?php bloginfo('home'); ?>/fotogalereya/" title="<?= $p->post_title; ?>">Галерея</a>
	&raquo;
	<!--<a href="<?= $p2->guid; ?>" title="<?= $p2->post_title; ?>"><?= $p2->post_title; ?></a>
	&raquo;-->
	<?php if (have_posts()):?>
		<?= $term->name; ?>
	<?php endif; ?>
</div>

<?php
	echo '<div id="content">';
		echo '<div class="big_s">';
		echo '<ul class="bxslider">';
			foreach ($myposts as $mypost){
				echo '<li class="pic_wrapper bxslider-block">';
					echo '<a href="'.wp_get_attachment_url( get_post_thumbnail_id($mypost->ID) ).'" rel="fancybox">';
						echo get_the_post_thumbnail($mypost->ID,'full');
					echo '</a>';;
				echo '</li>';
			}
		echo '</ul><!-- END bxslider-->';
		echo "</div>";
		
		
		$c = 0;
		echo '<div class="small_s">';
		echo '<div id="bx-pager" class="bxslider2">';
			foreach ($myposts as $mypost) {
				echo '<a class="min" data-slide-index="'.$c.'" href="">';
					echo get_the_post_thumbnail($mypost->ID,'full');
				echo '</a>';
				$c++;
			}
		echo '</div><!-- END bx-pager-->';
		echo "</div>";
	echo '</div>';
	
?>
<script src="<?php bloginfo('template_directory'); ?>/js/bxslider/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bxslider/jquery.bxslider.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bxslider/jquery.bxslider.js"></script>
<script>
	jQuery('.bxslider').bxSlider({
		auto: true,
		infiniteLoop: true,
		slideWidth: 706,
		minSlides: 1,
		maxSlides: 1,
		pagerCustom: '#bx-pager',
		speed: 2000,
		pause: 7000
	});
	jQuery('.bxslider2').bxSlider({
		auto: true,
		infiniteLoop: true,
		slideWidth: 210,
		moveSlides: 1,
		minSlides: 1,
		maxSlides: 3,
		slideMargin: 20,
		speed: 1000,
		pause: 7000,
	});
</script>

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>