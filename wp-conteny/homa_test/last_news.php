<?php/* Template Name: Остані новини */?>
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<div id="content">
<div id="contentinner">
	<div id="post-entry">
		<?php if (have_posts()):?>
			<? $post = get_post(get_the_ID());?>
			<div class="title">
				<h1><?= $post->post_title; ?></h1>
			</div><!-- POST INFO END -->
			<div class="page-content">
				<?php
					$cats = get_categories();
					foreach ($cats as $cat):
						if(6 != $cat->term_id && 1 != $cat->term_id && $cat->term_id != 5) homa_print_last_post($cat->slug,$cat->term_id, 1);
					endforeach;
				?>
			</div><!-- POST CONTENT END -->
			<div class="clearfix"></div>
		<?php endif; ?>
	</div><!-- POST ENTRY END -->
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
