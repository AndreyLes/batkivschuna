<?php/* Template Name: Галерея */?>
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<div id="content" class="gallery">
<div id="contentinner">
	<div id="post-entry">
		<?php if (have_posts()):?>
			<? $post = get_post(get_the_ID());?>
			<div class="title">
				<h1><?= $post->post_title; ?></h1>
			</div><!-- POST INFO END -->
			<div class="page-content">
				<?= $post->post_content; ?>
			</div><!-- POST CONTENT END -->
			<div class="clearfix"></div>
		<?php endif; ?>
	</div><!-- POST ENTRY END -->
	
	<?php
		/*  === * Функція виводить блок галерей * ===  */
		function homa_print_gallery($_id,$all_albom){
			$p = get_post($_id);
			echo '<div class="post-meta">';
				echo '<a class="image">'.get_the_post_thumbnail($_id, 'home-thumbnail').'</a>';
				echo '<h2><a href="'.get_post_permalink($_id).'" class="title">'.$p->post_title.'</a></h2>';
				echo '<a href="'.get_post_permalink($_id).'" class="show_all">'.$all_albom.'</a>';
				echo '<div class="clearfix"></div>';
			echo '</div>';
		}

		$all_albom = "Детальніше >>";
		$foto_id = 61;
		$video_id = 63;
		homa_print_gallery($foto_id,$all_albom);
		homa_print_gallery($video_id,$all_albom);
	?>
	
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
