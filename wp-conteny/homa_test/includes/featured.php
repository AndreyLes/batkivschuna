<?php $featured_category = get_theme_option('featured_category'); $featured_number = get_theme_option('featured_number'); ?>
<?php if(($featured_category == "Выберите рубрику:") || ($featured_number == 'Количество записей:')) { ?>
	<?php { /* nothing */ } ?>
<?php } else { ?>
<div id="featured">
	<div id="featured-title">Популярные новости</div>
	<div id="Gallerybox">
	
<script type="text/javascript">
function startGallery() {
	var myGallery = new gallery($('myGallery'), {
		timed: true,
		showArrows: true,
		showCarousel: false,
		embedLinks: true
	});
	document.gallery = myGallery;
}
window.onDomReady(startGallery);
</script>
	
		<div id="myGallery">
		<?php
			global $post;
			$category_id = get_cat_id($featured_category);
			$my_query = new WP_Query('cat='. $category_id . '&' . 'showposts='. $featured_number . '&' . 'orderby=date');
			while ($my_query->have_posts()) : $my_query->the_post();
			$do_not_duplicate = $post->ID;
			$the_post_ids = get_the_ID();
		?>

		<div class="imageElement post-<?php the_ID(); ?>">
			<?php if ( has_post_thumbnail() ) { ?>
				<?php the_post_thumbnail(array(800,9999), array('class' => 'full')); ?>
			<?php } else { ?>
				<img src="<?php echo get_featured_slider_image(); ?>" class="full" alt="<?php the_title(); ?>" />
			<?php } ?>
			<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php short_feat_title(); ?></a></h3>
			<p><?php the_featured_excerpt($excerpt_length=30); ?></p>
			<a href="<?php the_permalink(); ?>" title="open image" class="open"></a>
		</div><!-- IMAGE ELEMENT POST <?php the_ID(); ?> END -->

		<?php endwhile;?>

		</div><!-- MYGALLERY END -->
	</div><!-- GALLERBOX END -->
</div><!-- FEATURED END -->
<?php } ?> 