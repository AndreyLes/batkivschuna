<?php
	$category = get_the_category();
	$current_cat = $category[0]->cat_ID;
	$home_n = 'Головна';
?>
<?php if (is_single()) { ?>
	<div id="breadcrumbs">
		<a href="<?php bloginfo('home'); ?>" title="<?= $home_n; ?>"><?= $home_n; ?></a>
		<?php $p = get_post(35);?>
		&raquo;
		<!--a href="<?= $p->guid; ?>" title="<?= $p->post_title; ?>"><?= $p->post_title; ?></a-->
		<?php
			$category = get_the_category();
			if ($category) {
				echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
			} 
		?>
		&raquo;
		<?php the_title(); ?>
	</div>
<?php } else if (is_home()) { ?>
	<div id="breadcrumbs">Ласкаво просимо на сайт&nbsp;: "<a href="<?php bloginfo('home'); ?>" title="<?php bloginfo('description'); ?>"><?= $home_n; ?>"</a></div>
<?php } else if (is_category()) { ?>
	<div id="breadcrumbs">
		<?php $p = get_post(35);?>
		<a href="<?php bloginfo('home'); ?>" title="<?= $home_n; ?>"><?= $home_n; ?></a>
		&raquo;
		<!--<a href="<?php bloginfo('home'); ?>" title="<?= $p->post_title; ?>"><?= $p->post_title; ?></a>
		&raquo;-->
		<?php single_cat_title(); ?>
	</div>
<?php } else if (is_tag()) { ?>
	<div id="breadcrumbs">Ви знаходетесь тут&nbsp;: <a href="<?php bloginfo('home'); ?>" title="<?= $home_n; ?>"><?= $home_n; ?></a> &raquo; Метки рубрики <?php single_cat_title(); ?></div>
<?php } else if (is_page()) { ?>
	<div id="breadcrumbs"><a href="<?php bloginfo('home'); ?>" title="<?= $home_n; ?>"><?= $home_n; ?></a> &raquo; <?php the_title(); ?></div>
<?php } else if (is_archive()) { ?>
	<div id="breadcrumbs">Ви знаходетесь тут&nbsp;: <a href="<?php bloginfo('home'); ?>" title="<?= $home_n; ?>"><?= $home_n; ?></a> &raquo; <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
	<?php /* If this is a category archive */ if (is_day()) { ?>
		Архивы за день <?php the_time('F jS, Y'); ?>
	<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		Архивы за месяц <?php the_time('F Y'); ?>
	<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		Архивы за год <?php the_time('Y'); ?>
	<?php } ?>111</div>
<?php } else if (is_search()) { ?>
	<div id="breadcrumbs">Ви знаходетесь тут&nbsp;: <a href="<?php bloginfo('home'); ?>" title="<?= $home_n; ?>"><?= $home_n; ?></a> &raquo; Результаты поиска по запросу &quot; <?php the_search_query(); ?> &quot;</div>
<?php } else { ?>
<?php { /* nothing */ } ?>
<?php } ?>