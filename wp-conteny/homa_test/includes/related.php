<?php
$categories = get_the_category($post->ID);
if ($categories) {
	$category_ids = array();
	foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

	$args=array(
		'category__in' => $category_ids,
		'post__not_in' => array($post->ID),
		'showposts'=>5, // Number of related posts that will be shown.
		'caller_get_posts'=>1
	);

	$my_query = new wp_query($args);
	if( $my_query->have_posts() ) {
		echo '<li class="featured-category"><h6><span>Схожие записи</span></h6><div class="feat-cat-entry">';
		while ($my_query->have_posts()) {
			$my_query->the_post();
		?>
			
<div class="feat-cat-meta post-<?php the_ID(); ?>">
<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail(array(50,50), array('class' => 'alignleft')); ?>
<?php } ?>
<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
<span class="feat-cat-date"><?php the_time('l, F j, Y') ?>&nbsp;&nbsp;<?php edit_post_link('Редактировать'); ?></span>
<p><?php the_excerpt_feat_cat($excerpt_length=10); ?></p>
<div class="clearfix"></div>
</div><!-- FEATURED CATEGORY META <?php the_ID(); ?> END -->

			
		<?php
		}
		echo '</div></li>';
	}
}
?>