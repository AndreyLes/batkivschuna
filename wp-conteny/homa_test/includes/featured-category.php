<?php $featured_category = get_theme_option('featured_category_id1'); $featured_number = get_theme_option('featured_number1'); ?>
<?php if(($featured_category == 'Выберите рубрику:') || ($featured_number == 'Количество записей:')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<li class="featured-category">
<h6><span>Последние записи в <?php $cat_id = get_cat_id($featured_category); $category = get_cat_name($cat_id); echo $category; ?></span></h6>
<div class="feat-cat-entry">
<?php
global $post;
$category_id = get_cat_id($featured_category);
$my_query = new WP_Query('cat='. $category_id . '&' . 'offset=' . '&' . 'showposts='. $featured_number);
while ($my_query->have_posts()) : $my_query->the_post(); $do_not_duplicate = $post->ID; $the_post_ids = get_the_ID();
?>
<div class="feat-cat-meta post-<?php the_ID(); ?>">
<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail(array(50,50), array('class' => 'alignleft')); ?>
<?php } ?>
<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
<span class="feat-cat-date"><?php the_time('l, F j, Y') ?></span>
<p><?php the_excerpt_feat_cat($excerpt_length=10); ?></p>
<div class="clearfix"></div>
</div><!-- FEATURED CATEGORY META <?php the_ID(); ?> END -->
<?php endwhile;?> 
</div><!-- FEAT CAT ENTRY END -->
</li><!-- FEATURED CATEGORY END -->
<?php } ?>


<?php $featured_category = get_theme_option('featured_category_id2'); $featured_number = get_theme_option('featured_number2'); ?>
<?php if(($featured_category == 'Выберите рубрику:') || ($featured_number == 'Количество записей:')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<li class="featured-category">
<h6><span>Последние записи в <?php $cat_id = get_cat_id($featured_category); $category = get_cat_name($cat_id); echo $category; ?></span></h6>
<div class="feat-cat-entry">
<?php
global $post;
$category_id = get_cat_id($featured_category);
$my_query = new WP_Query('cat='. $category_id . '&' . 'offset=' . '&' . 'showposts='. $featured_number);
while ($my_query->have_posts()) : $my_query->the_post(); $do_not_duplicate = $post->ID; $the_post_ids = get_the_ID();
?>
<div class="feat-cat-meta post-<?php the_ID(); ?>">
<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail(array(50,50), array('class' => 'alignleft')); ?>
<?php } ?>
<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
<span class="feat-cat-date"><?php the_time('l, F j, Y') ?></span>
<p><?php the_excerpt_feat_cat($excerpt_length=10); ?></p>
<div class="clearfix"></div>
</div><!-- FEATURED CATEGORY META <?php the_ID(); ?> END -->
<?php endwhile;?> 
</div><!-- FEAT CAT ENTRY END -->
</li><!-- FEATURED CATEGORY END -->
<?php } ?>


<?php $featured_category = get_theme_option('featured_category_id3'); $featured_number = get_theme_option('featured_number3'); ?>
<?php if(($featured_category == 'Выберите рубрику:') || ($featured_number == 'Количество записей:')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<li class="featured-category">
<h6><span>Последние записи в <?php $cat_id = get_cat_id($featured_category); $category = get_cat_name($cat_id); echo $category; ?></span></h6>
<div class="feat-cat-entry">
<?php
global $post;
$category_id = get_cat_id($featured_category);
$my_query = new WP_Query('cat='. $category_id . '&' . 'offset=' . '&' . 'showposts='. $featured_number);
while ($my_query->have_posts()) : $my_query->the_post(); $do_not_duplicate = $post->ID; $the_post_ids = get_the_ID();
?>
<div class="feat-cat-meta post-<?php the_ID(); ?>">
<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail(array(50,50), array('class' => 'alignleft')); ?>
<?php } ?>
<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
<span class="feat-cat-date"><?php the_time('l, F j, Y') ?></span>
<p><?php the_excerpt_feat_cat($excerpt_length=10); ?></p>
<div class="clearfix"></div>
</div><!-- FEATURED CATEGORY META <?php the_ID(); ?> END -->
<?php endwhile;?> 
</div><!-- FEAT CAT ENTRY END -->
</li><!-- FEATURED CATEGORY END -->
<?php } ?>