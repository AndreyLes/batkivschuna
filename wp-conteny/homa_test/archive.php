<?php get_header(); ?>

<?php get_sidebar('left'); ?>

<div id="content">
<div id="contentinner">
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>

<div id="post-entry">

<?php $postcounter = 0; if (have_posts()) : ?>

<?php while (have_posts()) : $postcounter = $postcounter + 1; the_post(); ?>

<div class="post-meta-single" id="post-<?php the_ID(); ?>">
<div class="post-info-single">
<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
<div class="post-date-single">
Опубликовано <?php the_time('l, F j, Y') ?> - <?php the_author_posts_link(); ?>. В рубрике&nbsp;<?php the_category(', ') ?>&nbsp;&nbsp;<?php if(function_exists("the_tags")) : ?><?php the_tags('Метки:&nbsp;') ?><?php endif; ?>&nbsp;&nbsp;<?php edit_post_link('Редактировать'); ?>
</div><!-- POST DATE END -->
</div><!-- POST INFO END -->
<div class="post-content-single">
<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail(array(150,150), array('class' => 'alignleft')); ?>
<?php } ?>
<?php the_post_excerpt($excerpt_length=50); ?>
<?php /*<div class="readmore"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read More</a></div>*/?>
<div class="post-com"><?php comments_number('Комментариев нет', '1 комментарий', '% комментариев'); ?></div>
<div class="clearfix"></div>
</div><!-- POST CONTENT END -->
<div class="clearfix"></div>
</div><!-- POST META <?php the_ID(); ?> END -->

<?php $get_google_code = get_theme_option('adsense_loop'); if($get_google_code == '') { ?>
<?php } else { ?>
<?php if($postcounter <= 3){ ?>
<div class="adsense-loop2">
<?php echo stripcslashes($get_google_code); ?>
</div>
<?php } ?>
<?php } ?>

<?php endwhile; ?>

<?php else : ?>

<p class="center">Не найдено</p>

<p class="center">Извините, но по Вашему запросу ничего не было найдено.</p>

<?php endif; ?>

</div><!-- POST ENTRY END -->

<?php include (TEMPLATEPATH . '/includes/paginate.php'); ?>

</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>