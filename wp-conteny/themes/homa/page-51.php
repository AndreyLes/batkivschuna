<?php get_header(); ?>
<div id="breadcrumbs">
	<?php $p = get_post(30);?>
	<a href="<?php bloginfo('home'); ?>" title="Головна">Головна</a>
	&raquo;
	<a href="<?= $p->guid; ?>" title="<?= $p->post_title; ?>"><?= $p->post_title; ?></a>
	&raquo;
	<?php if (have_posts()):?>
		<?= get_the_title(); ?>
	<?php endif; ?>
</div>
<div id="content" class="biography">
<div id="contentinner" class="doc">
	<div id="post-entry">
		<?php if (have_posts()):?>
			<? $post = get_post(get_the_ID());?>
			<div class="title">
				<h1><?= $post->post_title; ?></h1>
			</div><!-- POST INFO END -->
			<div class="page-content">
				<?= $post->post_content; ?>
			</div><!-- POST CONTENT END -->
			<div class="clearfix"></div>
		<?php endif; ?>
	</div><!-- POST ENTRY END -->
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
