<?php if ((!function_exists("check_theme_footer") || !function_exists("check_theme_header"))) { ?><?php { /* nothing */ } ?><?php } else { ?>
<?php get_header(); ?>
<div id="breadcrumbs">
	<?php $p = get_post(37);?>
	<?php $p2 = get_post(61);?>
	<a href="<?php bloginfo('home'); ?>" title="Головна">Головна</a>
	&raquo;
	<a href="<?php bloginfo('home'); ?>/fotogalereya/" title="<?= $p->post_title; ?>">Галерея</a>
	&raquo;
	<!--<a href="<?php bloginfo('home'); ?>" title="<?= $p2->post_title; ?>"><?= $p2->post_title; ?></a>
	&raquo;-->
	<?php if (have_posts()):?>
		<?= get_the_title(); ?>
	<?php endif; ?>
</div>
<div id="content">
<div id="contentinner">
	<div class="single foto">
		<?php $post = get_post(get_the_ID()); ?>
		
		<div class="title">
			<h1><?= $post->post_title; ?></h1>
		</div>
			<?php homa_print_post($post->ID,true);?>
		<?php include (TEMPLATEPATH . '/includes/paginate.php'); ?>
	</div>
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?><?php } ?>