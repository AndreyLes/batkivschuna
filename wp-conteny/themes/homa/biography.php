<?php/* Template Name: Біографія */?>
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<div id="content" class="biography">
<div id="contentinner">
	<div id="post-entry">
		<?php if (have_posts()):?>
			<? $post = get_post(get_the_ID());?>
			<div class="title">
				<h1><?= $post->post_title; ?></h1>
			</div><!-- POST INFO END -->
			<div class="page-content">
				<?= $post->post_content; ?>
			</div><!-- POST CONTENT END -->
			<div class="clearfix"></div>
		<?php endif; ?>
	</div><!-- POST ENTRY END -->
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
