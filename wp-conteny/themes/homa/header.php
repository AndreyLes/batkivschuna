<?php
	/*id головной сторінки*/
	$home_id = 2;
	
	function pageURL(){
		$pageURL = ($_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
		if ( $_SERVER['SERVER_PORT'] != '80' )
			$pageURL .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'];
		else
			$pageURL .= $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		return $pageURL;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php function theme_footer_v() { if (!(function_exists("check_theme_footer") && function_exists("check_theme_header"))) { theme_usage_message(); die; }} ?>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php if(get_field('meta-title') != '') { ?>
	<meta name="title" content="<?php echo get_field('meta-title'); ?>" />
	<?php } else if(get_field('meta-title') == ''){ ?>
	<meta name="title" content="<?php echo meta_title(); ?>" />		
	<?php	} ?>
	<?php if(get_field('meta-description') != '') { ?>
		<meta name="description" content="<?php echo get_field('meta-description'); ?>" />
	<?php } 
	elseif(is_single()){ ?>
		<meta name="description" content="<?php echo homa_print_post_description(get_the_ID()); ?>" />
	<?php }?>
	<?php if(get_field('meta-keywords') != '') { ?>
	<meta name="keywords" content="<?php echo get_field('meta-keywords'); ?>" />
	<?php }elseif(is_single()){ ?>
	<meta name="keywords" content="<?php echo homa_print_post_keywords(get_the_ID(), 'post_tag'); ?>" />
	<?php }?>
	<meta property="og:type" content="article" >
	<meta property="og:site_name" content="batkivshchyna" >
	<meta property="og:title" content="" >
	<meta property="og:url" content="<?php echo pageURL(); ?>" >
	<?php if (is_single()) { ?>
		<meta property="og:image" content='<?php echo get_the_post_thumbnail_src(get_the_post_thumbnail(get_the_ID(), array(500,500))); ?>' >
	<?php } else { ?>
		<meta property="og:image" content='http://<?echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/homa/images/logo_img.png'>
	<?php } ?>
	<meta property="og:description" content="" >
	<meta charset="utf-8">
	<!--<?php bloginfo('template_directory'); ?>/images/rodina.png-->
	<!--title><?php echo meta_title(); ?></title-->
	<title><?php
		if(get_field('meta-title') != '') {
			echo get_field('meta-title');
		} else if(get_field('meta-title') == ''){
			echo meta_title();
		}
	?></title>
	
	<link href="<?php bloginfo('template_directory'); ?>/css/dropmenu.css" rel="stylesheet" type="text/css" />
	<link href="<?php bloginfo('template_directory'); ?>/css/sidebar_right.css" rel="stylesheet" type="text/css" />
	<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/ie.css" /><![endif]-->

	<link rel="icon" href="<?php bloginfo('template_directory'); ?>/images/icon.png" type="image/x-icon" />
	
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	<!-- Підключаємо файли да роботи блоком "Остані новини" -->
	<script src="<?php bloginfo('template_directory'); ?>/js/jquery.min.js"></script> <!-- подключаем последнюю версию jQuery -->
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/carousel.js"></script>
	<!-- Підключаємо файли да роботи блоком "Остані новини" -->

	<!-- bxSlider CSS file -->
	<link href="<?php bloginfo('template_directory'); ?>/js/bxslider/jquery.bxslider.css" rel="stylesheet" />
	<?/*<link href="<?php bloginfo('template_directory'); ?>/js/flexslider/jquery.flexslider.css" rel="stylesheet" />*/?>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bxslider/jquery.bxslider.js"/>
	
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/dropmenu.jquery.js"></script>
	
	<?php $featured_slider_activate = get_theme_option('featured_activate'); if($featured_slider_activate == 'Yes') { ?>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/mootools.v1.11.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jd.gallery.v2.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jd.gallery.set.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jd.gallery.transitions.js"></script>
	
	<?php } else { ?><?php { /* nothing */ } ?><?php } ?>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.zclip.js"></script>
	<?php remove_action( 'wp_head', 'wp_generator' ); ?>
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<?php wp_head(); ?>
	<?php $g_analytics = get_theme_option('google_analytics'); echo stripcslashes($g_analytics); ?>
	
	
	<script>
		jQuery(document).ready(function(){
			jQuery(window).on('scroll',function() {
				var scrolltop = jQuery(this).scrollTop();
			
				var nav = jQuery("#navigation");
				if(scrolltop >= nav.offset().top + nav.height()) {
					jQuery('#navigation-scroll').fadeIn(250);
				}
				else {
					jQuery('#navigation-scroll').fadeOut(250);
				}
			});
		});
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-73396815-1', 'auto');
	  ga('send', 'pageview');

	</script>
</head>

<body <?php body_class(); ?>>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter35150960 = new Ya.Metrika({
                    id:35150960,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/35150960" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?// exit('подождите, идет обновление сайта...'); ?>
<div id="top-wrapper">
	<div id="siteinfo">
		<div class="innerwrap">
		<div id="block_head_flag">
			<span style="text-transform:uppercase;font-family: arial; font-weight:bold; color: #000; font-size: 20px; display: block; padding: 5px;"><?= get_field('slog', $home_id); ?></span>
			<!--img src="<?php bloginfo('template_directory');?>/images/head_flag.png" id="head_flag"-->
		</div>
		<!--img src="<?php bloginfo('template_directory');?>/images/head_foto.png" id="head_foto"-->
			<h1 id="site-title" >
				<span class="name-hom"><?php// bloginfo('name');?></span>
				<span class="name-homeee" ><a style="display: block; width: 100%; height: 52%;" href="<?php bloginfo('url'); ?>"></a></span>
				<span style="text-transform:uppercase; left: 48px; top: 85px;"><i><?= get_field('text_header', $home_id); ?></i></span>
				<span style="left: 27px;
    top: 107px;
    font-style: italic;
    text-transform: uppercase;
    font-family: arial;
    font-weight: bold;
    color: #000;
    font-size: 20px;"><?= get_field('text_header2', $home_id); ?></span>
				<!--span><?= get_field('slog', $home_id); ?></span>
				<span><?= get_field('text_header2', $home_id); ?></span-->
			</h1>
		</div><!-- INNERWRAP END -->
		
		<!-- TOPBANNER END -->
		<div class="clearfix"></div>
	</div><!-- SITEINFO END -->
	
	<div id="navigation">
		<div class="innerwrap">
			<?php if ( function_exists( 'wp_nav_menu' ) ) { // Added in 3.0 ?>
				<?php wp_nav_menu( array(
				'theme_location' => 'primary',
				'menu' => 'Головне меню сайта',
				'container' => false, 
				'menu_id' => 'dropmenu',
				'fallback_cb' => 'revert_wp_menu_page'
				)); ?>
				<div class="up">
					<?php wp_nav_menu( array(
					'theme_location' => 'primary',
					'menu' => 'Головне меню сайта',
					'container' => false, 
					'menu_id' => 'dropmenu',
					'fallback_cb' => 'revert_wp_menu_page'
					)); ?>
				</div>
			<?php } else { ?>
				<ul id="dropmenu">
					<li id="<?php if (is_home()) { ?>home<?php } else { ?>page_item<?php } ?>"><a href="<?php bloginfo('url'); ?>" title="Home">Головна</a></li>
					<?php wp_list_pages('title_li=&depth=0&sort_column=menu_order'); ?>
				</ul>
				<!-- DROPMENU END -->
			<?php } ?>
			<div class="clearfix"></div>
		</div><!-- INNERWRAP END -->
	</div><!-- NAVIGATION END -->
	
	<div id="navigation-scroll" style="display: none;">
		<div class="innerwrap">
			<?php if ( function_exists( 'wp_nav_menu' ) ) { // Added in 3.0 ?>
				<?php wp_nav_menu( array(
				'theme_location' => 'primary',
				'menu' => 'Головне меню сайта',
				'container' => false, 
				'menu_id' => 'dropmenu',
				'fallback_cb' => 'revert_wp_menu_page'
				)); ?>
				<div class="up">
					<?php wp_nav_menu( array(
					'theme_location' => 'primary',
					'menu' => 'Головне меню сайта',
					'container' => false, 
					'menu_id' => 'dropmenu',
					'fallback_cb' => 'revert_wp_menu_page'
					)); ?>
				</div>
			<?php } else { ?>
				<ul id="dropmenu">
					<li id="<?php if (is_home()) { ?>home<?php } else { ?>page_item<?php } ?>"><a href="<?php bloginfo('url'); ?>" title="Home">Головна</a></li>
					<?php wp_list_pages('title_li=&depth=0&sort_column=menu_order'); ?>
				</ul><!-- DROPMENU END -->
			<?php } ?>
			<div class="clearfix"></div>
		</div><!-- INNERWRAP END -->
	</div><!-- NAVIGATION-SCROLL END -->
</div>
<div id="wrapper">
	<div id="container">
<div id="header">
	
</div><!-- HEADER END -->
<?php
if($_SESSION['last_news']){
				$posts = $_SESSION['last_news'];
				last_news_javascript(10000);
			}
			else{
	$posts = get_posts(array(
				'numberposts' => -1,
				'orderby' => 'date',
				'order' => 'DESC',
				'post_type' => 'post'));
	$_SESSION['last_news'] = $posts;			
			}
	$speed = get_field('speed_text',$home_id);
	$text = get_field('last_news_on_main',$home_id);
	//var_dump($post);
?>
<style>
#last_div .bxslider2 li {
	height: 30px;
}
#last_div .bx-wrapper {
	float: left;
    width: 970px !important;
    margin-top: -3px;
}
#last_div .bx-controls {
	display:none;
}
</style>
<div id="last_div">
	<span class="carousel-text"><a href="<?php bloginfo('url'); ?>/vsi_novini/"><?=$text;?> </a></span>
	<div class="carousel-button-left"><span id="arrow_left">&lt;</span></div>
	<div class="carousel-button-right" style=""><span id="arrow_right">&gt;</span></div>
	<ul style="visibility:hidden;" class="bxslider2">
		<? foreach($posts as $p): ?>
		<li><a href="<? echo get_permalink($p->ID);/*$p->guid;*/ ?>"><?  echo $p->post_title; ?></a></li>
		<? endforeach; ?>
	</ul>
	
</div><!-- LAST_HEWS END -->

<script>
$(window).load(function(){
	if($(window).width() >= 320 && $(window).width() <= 1199) {
		$('#navigation .innerwrap #dropmenu, #navigation-scroll .innerwrap #dropmenu').append("<li class='pop'>Меню</li>");
	}
	
	$('ul.bxslider2').css("visibility","visible");
	$('.bxslider3').css("visibility","visible");
});
$(document).ready(function(){
	
	/*window.addEventListener("orientationchange", function() {
		location.reload();
	}, false);*/
	
	window.onload = function(){ 
		$('.pop').click(function() {
			$(this).parent().siblings('.up').slideToggle();
		});
	};
	
	$('#dropmenu > li > ul > li').hover(function(){
		left = $(this).width();
		$(this).find('ul').css('left', left + 'px').fadeIn();
	}, function(){
		$(this).find('ul').fadeOut();
	});
	
	$('.bxslider3').css("visibility","visible");
	$('ul.bxslider2').css("visibility","visible");
	$('.bxslider2').bxSlider({
		auto: true,
		autoControls: true,
		pause: 6000
	});
});
$('#arrow_left').click(function(){
	$('#last_div a.bx-prev').click();
});
$('#arrow_right').click(function(){
	$('#last_div a.bx-next').click();
});
</script>
<div id="main">
