<?php/* Template Name: Фотогалерея */?>
<?php $home_id = 2; ?>
<?php get_header(); ?>
<div id="breadcrumbs">
	<?php $p = get_post(37);?>
	<a href="<?php bloginfo('home'); ?>" title="Головна">Головна</a>
	&raquo;
	<!--<a href="<?php bloginfo('home'); ?>" title="<?= $p->post_title; ?>"><?= $p->post_title; ?></a>
	&raquo;-->
	<?php if (have_posts()):?>
		<?= get_the_title(); ?>
	<?php endif; ?>
</div>
<div id="content" class="list_gallery">
<div id="contentinner">
	<div id="post-entry">
		<? $post = get_post(get_the_ID());?>
		<div class="title">
			<h1><?= $post->post_title; ?></h1>
		</div><!-- POST INFO END -->
		<div class="page-content">
			<div class="foto_gallery">
			<?php
				$args = array(
					'type'						=> 'post'
					,'child_of'					=> 0
					,'parent'					=> ''
					,'orderby'					=> 'date'
					,'order'					=> 'DESC'
					,'hide_empty'				=> 1
					,'hierarchical'				=> 1
					,'exclude'					=> ''
					,'include'					=> ''
					,'number'					=> 0
					,'taxonomy'					=> 'albums'
					,'pad_counts'				=> false
					,'posts_per_page'			=> -1
				);
				$categories = get_categories( $args );

				$kil = 6;
				$kil_p = 1; //количество видимих страниц возле активной с лева и с права, при пагинации
				$_page = 1;
				$count = 0;
				if($_GET['pag']) $_page = $_GET['pag'];
				if((1 > $_page) || ((count($categories)/$kil) < $_page)) $_page = 1;

				$c = 0;
				if( $categories ){
					foreach( $categories as $cat ){
						$count++;
						if($count >= $kil*$_page-$kil+1 && $count < $kil*$_page+1){
							$c++;
							if(($c%3) == 1){echo '<div class="row">';}

							$taxonomy = $cat->taxonomy;
							$term_id = $cat->term_id;

							// load thumbnail for this taxonomy term (term string)
							$thumbnail = get_field('album_pic', $taxonomy . '_' . $term_id);
							
							$url_a = home_url( '/' ).'?'.$cat->taxonomy.'='.$cat->slug;
							$category_id = get_cat_ID( $cat->name );
							$category_link = get_category_link( $category_id );
							$img_url = get_field('album_pic');
							
							echo '<a class="album_wrapper" href="'.esc_url($url_a).'"><div class="album_pic_wrapper"><img src="'.$thumbnail['url'].'" alt="'.$cat->name.'" class="albums_pic" /></div><p class="tax_name">'.$cat->name.'</p></a>';

							if(!($c%3)){echo "</div>";}
						}
					}
					if($c%3){echo "</div>";}
				}
			?>
			</div>
		</div><!-- POST CONTENT END -->
		<div class="clearfix"></div>
	</div><!-- POST ENTRY END -->
	
	<?		
		/* Пагинация */
		/* В if исключаем ту сетуацию когда одна страница. */
		/* Пагинация не виводим когда одна страница. */
		if($count > $kil){
			$count /= $kil;
			$count = (int)ceil($count); /*Округляет дробь в большую сторону*/
	?>
			<div class="nomer_page">
			<?php
				/*В if проверяим нужны ли моментальные переходы на первою и последнею страницу*/
				if($count <= $kil_p + 1)
					for($i = 1; $i < $count+1; $i++) {
						echo "<a href='?pag=$i'";
						if($i == $_page) echo'class="select"';
						echo ">$i</a>";
					}
				/*В else если переходы всеже нужны*/
				else{
					/*Добавляем переход на первою страницу*/
					if(1 < $_page - $kil_p){
						echo "<a href='?pag=1'>1</a>";
						if(1 != $_page - $kil_p - 1){echo "<span>...</span>";}
					}
					for($i = 1; $i < $count+1; $i++) {
						if($i <= $_page + $kil_p && $i >= $_page - $kil_p){
							echo "<a href='?pag=$i'";
							if($i == $_page)echo 'class="select"';
							echo ">$i</a>";
						}
					}
					/*Добавляем переход на последнею страницу*/
					if($count > $_page + $kil_p) {
						if($count != $_page + $kil_p + 1)
							echo "<span>...</span>";
						echo "<a href='?pag=$count'>$count</a>";
					}
				}
			?>
			</div>
	<?php
		}
	?>
	
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
