<?php if (is_single()) { ?>
<div id="post-navigator-single">
<div class="left"><?php previous_post_link('&laquo;&nbsp;%link') ?></div>
<div class="right"><?php next_post_link('%link&nbsp;&raquo;') ?></div>
<div class="clearfix"></div>
</div>

<?php } else if (is_page()) { ?>

<div id="post-navigator">
<?php link_pages('<strong>Страница</strong> ', '', 'number'); ?>
<div class="clearfix"></div>
</div>

<?php } else if (is_tag()) { ?>

<div id="post-navigator">
<div class="right"><?php next_posts_link('Предыдущие записи &laquo; '); ?></div>
<div class="left"><?php previous_posts_link('&raquo; Следующие записи'); ?></div>
<div class="clearfix"></div>
</div>

<?php } else { ?>

<div id="post-navigator">
<?php if (function_exists('custom_wp_pagenavi')) : ?>
<?php custom_wp_pagenavi(); ?>
<?php else : ?>
<div class="left"><?php posts_nav_link('',__('&laquo; Следующие записи'),'') ?></div>
<div class="right"><?php posts_nav_link('','',__('Предыдущие записи &raquo;')) ?></div>
<?php endif; ?>
<div class="clearfix"></div>
</div>

<?php } ?>