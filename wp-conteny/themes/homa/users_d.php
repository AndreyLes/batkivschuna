<?php/* Template Name: Кандидати (Депутати) */?>
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<div id="content" class="biography kandedats">
<div id="contentinner">
	<div id="post-entry">
		<?php 
		if (have_posts()):?>
			<? $post = get_post(get_the_ID()); ?>
			<div class="title">
				<h1><?= $post->post_title; ?></h1>
			</div><!-- POST INFO END -->
			<div class="page-content">
				<?= $post->post_content; ?>
			</div><!-- POST CONTENT END -->
			<div class="clearfix"></div>
		<?php endif; ?>
		
		<? $images = get_field('all_picturesss', get_the_ID());
		preg_match_all('/<img[^>]+>/i',$images, $result);
		$img = array();
		foreach( $result as $key => $img_tag)
		{
			
			foreach($img_tag as $key=>$imggg){
			  preg_match_all('/(alt|title|src)=("[^"]*")/i',$imggg, $img[$key]);
			 }
		}
		foreach($img as $image){?>
		<div class="imagesss">
			<div style="height: 170px;">
				<a href="<? echo substr($image[2][0], 1, -1); ?>" rel="fancybox"><img width="200" src="<? echo substr($image[2][0], 1, -1); ?>"></a>
			</div>
			<div style="text-align:center;"><h2><? echo substr($image[2][1], 1, -1); ?></h2></div>
		 </div>
		 
		 <?
		 $i++;
		}
		?>
		
	</div><!-- POST ENTRY END -->
<script>
/*$(document).ready(function(){
	num = 0;
	image = ""
	$('.image_all_pl a').each(function(){
		image = $(this).html();
		if(num == 0){
			image += "<div><div style='float:left; margin-right:15px; width:200px; height:150px; overfollow:hidden;'>"+image+"</div></div>"
		}
		else if(num/3 == 0){
			image += ""
		}
		
	});
});*/
</script>
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->
<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
