<?php if ((!function_exists("check_theme_footer") || !function_exists("check_theme_header"))) { ?><?php { /* nothing */ } ?><?php } else { ?>
<?php get_header(); ?>
<?php/* get_sidebar('left'); */?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<div id="content">
<div id="contentinner">
	<div class="single">
		<?php $post = get_post(get_the_ID());
		//var_dump($post);?>
		<div class="title">
			<h1><?= $post->post_title; ?></h1>
		</div>
		<div class="hid_img" style="display:none;"><?php echo get_the_post_thumbnail(get_the_ID(), array(500,500)); ?></div>
		<?php homa_print_post($post->ID,true);?>
		
		<div id="single_paginate">
			<?php homa_paginate_single($post->ID); ?>
		</div>
	</div>
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->
<script>
	/* 	src = $('.hid_img img').attr('src');
		$('html').find('meta[property="og:image"]').attr("content", src); */
</script>
<?php get_sidebar('right'); ?>

<div class="hid_img" style="display:none;"><?php echo get_the_post_thumbnail(get_the_ID(), array(500,500)); ?>
<script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir" data-yashareTheme="counter"></div>
</div>

<?php get_footer(); ?><?php } ?>