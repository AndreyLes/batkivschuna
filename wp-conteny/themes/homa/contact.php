<?php/* Template Name: Зворотній зв'язок */?>
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>
<!--script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/js.js"/></script-->

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.maskedinput.js"/></script>

<div id="content" class="contact">
<div id="contentinner">
	<? $post = get_post(get_the_ID());?>
	<div class="title">
		<h1><?= $post->post_title; ?></h1>
	</div><!-- POST INFO END -->
	<div class="page-content">
		<?= $post->post_content; ?>
	</div><!-- POST CONTENT END -->
	<? if(get_field('see_form',get_the_ID())) {  ?>
	<div style="margin-top: 25px;" class="title">
		<h1><? the_field('title_zv',get_the_ID()); ?></h1>
	</div><!-- POST INFO END -->
	
	<div id="contact_form"><?php echo do_shortcode( '[contact-form-7 id="80" title="Зворотній звязок"]' ); ?></div>
	
		
	<? }?>
	
	
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->
<script>
	/*$(document).ready(function(){
		$("#contact_form input[name='tel']").mask("+38(099) 999-99-99?");
	});*/
</script>
<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
