Thanks for using TRITONE WordPress theme.

If you don't know how to install this theme on your WordPress, please visit below article:

http://www.themelab.com/2008/03/02/how-to-install-a-wordpress-theme/


THIS THEME IS TESTED ON THE LATEST VERSION OF WORDPRESS, FIREFOX AND IE
!! In order for this theme to work properly, please make sure your server has the latest PHP version installed. Thanks. !!
--------------------------------------------------------------------------------------------------------------------------
HOW TO USE THE FEATURED CONTENT SLIDER
----------------------------------------------------------------------------------------------------

- After you activated the theme, please make sure you go to the theme option and setup your site there.

- To use the featured content slider, enable it, choose a category from the dropdown options, choose how many posts to show and save it. (note: please make sure you have more than 1 post on the category for slider).

- Please refer to below "POSTING WITH IMAGES" on how to manually define the image for slider.

-----------------------------------------------------------------------------------
POSTING WITH IMAGES
----------------------------------------------------------------------------------------------------

1. When creating a new post, look for the lower right corner. You should see a "Featured Image" section there.

2. In the "Featured Image" section, upload an image or select from gallery. Then choose "set as featured image".

3. Save the Post and viola! ;)

Note: To avoid low quality images, always upload high-resolution images in the gallery. If the images doesn't crop with the right sizes, I suggest you use the plugin "Regenerate Thumbnails" (download at http://wordpress.org/extend/plugins/regenerate-thumbnails/). The reason for that is because the new post-thumbnail feature only works fully for new image uploads.

-----------------------------------------------------------------------------------------------------


That's all. If you need any help using this theme, please visit the theme page, http://www.magpress.com/wordpress-themes/tritone.html and post your question there or email me at webmaster@magpress.com.

*Please make sure that you keep all footer links intact because any modification on the links will cause your site to stop working.

If you're interested in purchasing a developer's license (clean footer version) for this theme, please contact me at webmaster@magpress.com. Thanks.


Thanks.

Regards,
Ronald KSY
(MagPress.com)

----------------------------------------------------------------------------------------------------