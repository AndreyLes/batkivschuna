<?php/* Template Name: Відеогалерея */?>
<?php $home_id = 2; ?>
<?php get_header(); ?>
<div id="breadcrumbs">
	<?php $p = get_post(37);?>
	<a href="<?php bloginfo('home'); ?>" title="Головна">Головна</a>
	&raquo;
	
	<?php if (have_posts()):?>
		<?= get_the_title(); ?>
	<?php endif; ?>
</div>
<div id="content" class="video_gallery">
<div id="contentinner">
	<div style="text-align:center;" id="post-entry">
		<?php if (have_posts()):?>
			<? $post = get_post(get_the_ID());?>
			<div class="title">
				<h1><?= $post->post_title; ?></h1>
			</div><!-- POST INFO END -->
			<div class="page-content">
				<?= $post->post_content; ?>
			</div><!-- POST CONTENT END -->
			<div class="clearfix"></div>
		<?php endif; ?>
	</div><!-- POST ENTRY END -->

	<?

		$kil = 6;
		$kil_p = 1;	//количество видимих страниц возле активной с лева и с права, при пагинации
		$_page = 1;
		$count = 0;
		if($_GET['pag']) $_page = $_GET['pag'];

		$posts = null;
		$posts = get_posts(array(
					'numberposts' => -1,
					'post_type' => "video"
					));

		$c = 0;
		foreach($posts as $elem){
			$count++;
			if($count >= $kil*$_page-$kil+1 && $count < $kil*$_page+1){
				$c++;
				$elem_id = $elem->ID;
				if(($c%3) == 1)echo '<div style="text-align:center;"><div id="elem'.$elem_id.'" class="block_elem_video">';
				else echo '<div id="elem'.$elem_id.'" class="block_elem_video par">';
					if(has_post_thumbnail($elem_id)) {
						echo '<div class="image">';
							echo '<a href="'.get_the_permalink($elem_id).'" rel="fancybox" title="'.get_the_title($elem_id).'" alt="'.get_the_title($elem_id).'">'.get_the_post_thumbnail($elem_id, 'home-thumbnail', array('class' => 'aligncenter')).'</a>';
						echo '</div>';
					}
					else
						echo '<div class="image"><a href="'.get_the_permalink($elem_id).'"><img src="'.get_bloginfo('template_directory').'/images/if_not_news_image.jpg" height="200" width="285"/></a></div>';
					echo '<h2><a href="'.get_the_permalink($elem_id).'">'.get_the_title($elem_id).'</a></h2>';
				echo '</div><!-- END ELEM VIDEO -->';
				if(!($c%3))echo '</div>';
			}
		}
		if($c%3)echo '</div>';

		/* Пагинация */
		/* В if исключаем ту сетуацию когда одна страница. */
		/* Пагинация не виводим когда одна страница. */
		if($count > $kil){
			$count /= $kil;
			$count = (int)ceil($count); /*Округляет дробь в большую сторону*/
	?>
			<div class="nomer_page">
			<?php
				/*В if проверяим нужны ли моментальные переходы на первою и последнею страницу*/
				if($count <= $kil_p + 1)
					for($i = 1; $i < $count+1; $i++) {
						echo "<a href='?pag=$i'";
						if($i == $_page) echo'class="select"';
						echo ">$i</a>";
					}
				/*В else если переходы всеже нужны*/
				else{
					/*Добавляем переход на первою страницу*/
					if(1 < $_page - $kil_p){
						echo "<a href='?pag=1'>1</a>";
						if(1 != $_page - $kil_p - 1){echo "<span>...</span>";}
					}
					for($i = 1; $i < $count+1; $i++) {
						if($i <= $_page + $kil_p && $i >= $_page - $kil_p){
							echo "<a href='?pag=$i'";
							if($i == $_page)echo 'class="select"';
							echo ">$i</a>";
						}
					}
					/*Добавляем переход на последнею страницу*/
					if($count > $_page + $kil_p) {
						if($count != $_page + $kil_p + 1)
							echo "<span>...</span>";
						echo "<a href='?pag=$count'>$count</a>";
					}
				}
			?>
			</div>
	<?php
		}
	?>
	
</div><!-- CONTENTINNER END -->
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
