<?php if ((!function_exists("check_theme_footer") || !function_exists("check_theme_header"))) { ?><?php { /* nothing */ } ?><?php } else { ?>
<?php get_header(); ?>

<div id="breadcrumbs">
	<?php $p = get_post(37);?>
	<?php $p2 = get_post(63);?>
	<a href="<?php bloginfo('home'); ?>" title="Головна">Головна</a>
	&raquo;
	<a href="<?php bloginfo('home'); ?>/videogalereya/" title="<?= $p->post_title; ?>">Відео</a>
	&raquo;
	<!--<a href="" title="<?= $p2->post_title; ?>"><?= $p2->post_title; ?></a><!--<= //$p2->guid; >>
	&raquo;-->
	<?php if (have_posts()):?>
		<?= get_the_title(); ?>
	<?php endif; ?>
</div>
<div id="content">
<div id="contentinner">
	<div class="single video">
		<?php $post = get_post(get_the_ID()); ?>
		
		<div class="title">
			<h1><?= $post->post_title; ?></h1>
		</div>
		<?php homa_print_post($post->ID,true);?>
	</div>
</div><!-- CONTENTINNER END -->
	<?php

		$posts = get_posts(array(
			'post_type' => 'video',
			'post_per_page' => 3,
			'order' => 'DESC',
			'orderby' => 'date'
		));
		$c = 3;
		echo "<div>";
		foreach($posts as $elem){
			$elem_id = $elem->ID;
			if(get_the_ID() !== $elem_id){
				echo '<div id="elem'.$elem_id.'" class="block_elem_video">';
					$tit = get_the_title($elem_id);
					if(has_post_thumbnail($elem_id)) {
						echo '<div class="image">';
							echo '<a href="'.get_the_permalink($elem_id).'" rel="fancybox" title="'.$tit.'" alt="'.get_the_title($elem_id).'">'.get_the_post_thumbnail($elem_id, 'home-thumbnail', array('class' => 'aligncenter')).'</a>';
						echo '</div>';
					}
					else
						echo '<div class="image"><a href="'.get_the_permalink($elem_id).'" title="'.$tit.'"><img src="'.get_bloginfo('template_directory').'/images/if_not_news_image.jpg" height="200" width="285"/></a></div>';
					echo '<h2><a href="'.get_the_permalink($elem_id).'" title="'.$tit.'">';
					if(30 < strlen($tit) && 0 < strlen($tit))echo mb_substr($tit,0,30,'UTF-8')."...";
					echo '</a></h2>';
				echo '</div><!-- END ELEM VIDEO -->';
				if(!--$c)break;
			}
		}
		echo '</div>';
	?>
</div><!-- CONTENT END -->

<?php get_sidebar('right'); ?>

<?php get_footer(); ?><?php } ?>