<?php
/*
Template Name: One column, no sidebar
*/
?>

<?php get_header(); ?>

<div id="fullcontent">
<?php include (TEMPLATEPATH . '/includes/breadcrumbs.php'); ?>

<div id="post-entry">

<?php if (have_posts()) : ?>

<?php while (have_posts()) : the_post(); ?>

<div class="post-meta-single" id="post-<?php the_ID(); ?>">
<div class="post-info-single">
<h1><?php the_title(); ?></h1>
</div><!-- POST INFO END -->
<div class="post-content-single">
<?php the_content(); ?>
<?php wp_link_pages('before=<div id="page-links">&after=</div>'); ?>
</div><!-- POST CONTENT END -->
<div class="clearfix"></div>
</div><!-- POST META <?php the_ID(); ?> END -->

<?php endwhile; ?>

<?php comments_template( '', true ); ?>

<?php else : ?>

<p class="center">Не найдено</p>

<p class="center">Извините, но по Вашему запросу ничего не было найдено.</p>

<?php endif; ?>

</div><!-- POST ENTRY END -->

<?php include (TEMPLATEPATH . '/includes/paginate.php'); ?>

</div><!-- CONTENT END -->

<?php get_footer(); ?>